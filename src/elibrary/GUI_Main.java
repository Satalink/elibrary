package elibrary;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipFile;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.plaf.SplitPaneUI;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import org.pdfbox.pdmodel.PDDocument;

public class GUI_Main extends JFrame implements ActionListener {
    private Interface main;
    public Font font = new java.awt.Font("Georgia", 1, 12);
    protected File fromFile;
    protected String[] fromFiles;
    protected boolean delbool = false; 
    protected int threadCount = Thread.activeCount();
    protected Color[] colors;
    public Map<String, Color> colorMap;
    public Map<String, Font> fontMap;
    protected Map configs;

    public GUI_Main(Interface iface) {
	main = iface;	
    colors = main.getColors();
    colorMap = new HashMap<String, Color>();
        colorMap.put("black", new Color(0,0,0));
        colorMap.put("white", new Color(255,255,255));
        colorMap.put("header", new Color(200,200,200));
        colorMap.put("menu", new Color(220,220,220));
        colorMap.put("skyblue", new Color(135,206,235));
        colorMap.put("cloudwhite", new Color(209,238,238));
    fontMap = new HashMap<String, Font>();
        fontMap.put("GA10", new Font("Georgia", 0, 10));
        fontMap.put("GA10B", new Font("Georgia", 1, 10));
        fontMap.put("GA11", new Font("Georgia", 0, 11));
        fontMap.put("GA11B", new Font("Georgia", 1, 11));
        fontMap.put("GA12B", new Font("Georgia", 1, 12));
        fontMap.put("GA14", new Font("Georgia", 0, 14));
        fontMap.put("GA14B", new Font("Georgia", 1, 14));
        fontMap.put("GA16", new Font("Georgia", 0, 16));
        fontMap.put("GA16B", new Font("Georgia", 1, 16));	
    initComponents();
	//Window Exit Listener
	addWindowListener(new WindowAdapter() {
	    @Override
	public void windowClosing(WindowEvent ex) {
	main.exitApp();
	}
	});
	//Mouse Listener
	MouseWheelListener imagePanelMouseWheelListener = new MouseAdapter() {
	    public void mouseWheeled(MouseWheelEvent mw) {
		
	    }
	};
	
	MouseListener jTree1MouseListener = new MouseAdapter() {
	    @Override
		public void mousePressed(MouseEvent mc) {
			int selRow = jTree1.getRowForLocation(mc.getX(), mc.getY());
			TreePath selPath = jTree1.getPathForLocation(mc.getX(), mc.getY());
			if(selRow != -1) {
		if(mc.getButton() == MouseEvent.BUTTON1) {
					if(mc.getClickCount() == 1) {
						 jTree1SingleClick(selPath);
					} else if(mc.getClickCount() == 2) {
						main.setCursor("Wait");
			jTree1DoubleClick(selPath, false);
					}
		}
			}
		}
	};
	MouseListener jTable1MouseListener = new MouseAdapter() {
	    @Override
            public void mousePressed(MouseEvent mc) {
                if(mc.getButton() == MouseEvent.BUTTON1) {
                    if(mc.getClickCount() == 1) {
                        String filePath = jTable1.getModel().getValueAt(jTable1.getSelectedRow(), 4).toString(); 
                        jTable1SingleClick(filePath);
                    } else if(mc.getClickCount() == 2) {
                        File file = new File(jTable1.getModel().getValueAt(jTable1.getSelectedRow(), 4).toString());
                        if (file.isFile()) {
                            jTable1DoubleClick(file.getAbsolutePath(), false);
                        } else if (file.isDirectory()) {
                            createjTable(file);
                        }
                    }
                } else if(mc.getButton() == MouseEvent.BUTTON3 && jTable1.getSelectedRowCount() < 2) {
                    Point coords = mc.getPoint();
                    int rowNumber = jTable1.rowAtPoint(coords);
                    jTable1.getSelectionModel().setSelectionInterval(rowNumber, rowNumber);
                }
            }
    	};
	jTable1.addMouseListener(jTable1MouseListener);
    imagePanel.addMouseWheelListener(imagePanelMouseWheelListener);
	jTree1.addMouseListener(jTree1MouseListener);
	jTree1.setCellRenderer(new JTreeToolTipRenderer());

        // Create Popup Menus
	createimagePanelPopupMenu();
	createjTreePopupMenu();
        createjTablePopupMenu();
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenuTree = new javax.swing.JPopupMenu();
        jPopupMenuTable = new javax.swing.JPopupMenu();
        jPopupMenuimagePanel = new javax.swing.JPopupMenu();
        jSplitPane1 = new javax.swing.JSplitPane();
        jSplitPane2 = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        Tab1Panel = new javax.swing.JPanel();
        readCheckBox = new javax.swing.JCheckBox();
        titleTextField = new javax.swing.JTextField();
        titleLable = new javax.swing.JLabel();
        authorComboBox = new javax.swing.JComboBox();
        authorLabel = new javax.swing.JLabel();
        publisherLabel = new javax.swing.JLabel();
        publisherComboBox = new javax.swing.JComboBox();
        editionLabel = new javax.swing.JLabel();
        editionSpinner = new javax.swing.JSpinner();
        languageLabel = new javax.swing.JLabel();
        languageComboBox = new javax.swing.JComboBox();
        pubdateLabel = new javax.swing.JLabel();
        pubdateTextField = new javax.swing.JTextField();
        ratingSlider = new javax.swing.JSlider();
        notesScrollPane = new javax.swing.JScrollPane();
        notesTextArea = new javax.swing.JTextArea();
        imagePanel = new javax.swing.JPanel();
        pagesLabel = new javax.swing.JLabel();
        pagesTextField = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jProgressBar1 = new javax.swing.JProgressBar();
        MsgArea = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        MenuFile = new javax.swing.JMenu();
        SetCatRoot = new javax.swing.JMenuItem();
        Exit = new javax.swing.JMenuItem();
        MenuOptions = new javax.swing.JMenu();
        ColorMenu = new javax.swing.JMenuItem();
        MenuDatabase = new javax.swing.JMenu();
        dbClean = new javax.swing.JMenuItem();
        dbConnect = new javax.swing.JMenuItem();
        dbDisconnect = new javax.swing.JMenuItem();
        MenuHelp = new javax.swing.JMenu();
        Debug = new javax.swing.JCheckBoxMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("eLibrary");
        setForeground(java.awt.Color.white);
        setIconImage(getIconImage());
        setLocationByPlatform(true);
        setMinimumSize(new java.awt.Dimension(200, 200));

        jSplitPane1.setBackground(new java.awt.Color(204, 204, 204));
        jSplitPane1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jSplitPane1.setOneTouchExpandable(true);

        jSplitPane2.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane2.setLastDividerLocation(300);
        jSplitPane2.setOneTouchExpandable(true);

        jTable1.setFont(new java.awt.Font("Georgia", 1, 11));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                " ", "Name", "Size", "Modified On"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setColumnSelectionAllowed(true);
        jTable1.setDragEnabled(true);
        jScrollPane2.setViewportView(jTable1);
        jTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jTable1.getColumnModel().getColumn(0).setResizable(false);

        jSplitPane2.setLeftComponent(jScrollPane2);

        jTabbedPane1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        readCheckBox.setText(" Read");

        titleLable.setText("Title");

        authorLabel.setText("Author");

        publisherLabel.setText("Publisher");

        editionLabel.setText("Edition");

        languageLabel.setText("Language");

        pubdateLabel.setText("Publication Date");

        ratingSlider.setFont(new java.awt.Font("Georgia", 1, 12));
        ratingSlider.setMaximum(5);
        ratingSlider.setMinorTickSpacing(1);
        ratingSlider.setPaintLabels(true);
        ratingSlider.setPaintTicks(true);
        ratingSlider.setSnapToTicks(true);
        ratingSlider.setToolTipText("Rating");
        ratingSlider.setValue(3);
        ratingSlider.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        notesTextArea.setColumns(20);
        notesTextArea.setRows(5);
        notesScrollPane.setViewportView(notesTextArea);

        imagePanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        imagePanel.setPreferredSize(new java.awt.Dimension(128, 161));

        javax.swing.GroupLayout imagePanelLayout = new javax.swing.GroupLayout(imagePanel);
        imagePanel.setLayout(imagePanelLayout);
        imagePanelLayout.setHorizontalGroup(
            imagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 122, Short.MAX_VALUE)
        );
        imagePanelLayout.setVerticalGroup(
            imagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 155, Short.MAX_VALUE)
        );

        pagesLabel.setText("Pages");

        pagesTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        pagesTextField.setMaximumSize(new java.awt.Dimension(20, 75));

        javax.swing.GroupLayout Tab1PanelLayout = new javax.swing.GroupLayout(Tab1Panel);
        Tab1Panel.setLayout(Tab1PanelLayout);
        Tab1PanelLayout.setHorizontalGroup(
            Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Tab1PanelLayout.createSequentialGroup()
                .addGroup(Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(imagePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(readCheckBox)
                    .addComponent(ratingSlider, 0, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(Tab1PanelLayout.createSequentialGroup()
                        .addGroup(Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(Tab1PanelLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(titleLable, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(publisherLabel)
                                .addComponent(authorLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(Tab1PanelLayout.createSequentialGroup()
                                .addGroup(Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(publisherComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(authorComboBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(49, 49, 49)
                                .addGroup(Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(Tab1PanelLayout.createSequentialGroup()
                                        .addComponent(editionLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(editionSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(languageLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(languageComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(Tab1PanelLayout.createSequentialGroup()
                                        .addComponent(pubdateLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(pubdateTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE))))
                            .addComponent(titleTextField))
                        .addGap(18, 18, 18)
                        .addComponent(pagesLabel)
                        .addGap(18, 18, 18)
                        .addComponent(pagesTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE))
                    .addComponent(notesScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 646, Short.MAX_VALUE))
                .addContainerGap())
        );
        Tab1PanelLayout.setVerticalGroup(
            Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Tab1PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tab1PanelLayout.createSequentialGroup()
                        .addComponent(imagePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ratingSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(readCheckBox)
                        .addGap(51, 51, 51))
                    .addGroup(Tab1PanelLayout.createSequentialGroup()
                        .addGroup(Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(titleLable)
                            .addComponent(titleTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pagesLabel)
                            .addComponent(pagesTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(authorLabel)
                            .addComponent(authorComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(editionLabel)
                            .addComponent(editionSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(languageLabel)
                            .addComponent(languageComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(Tab1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(publisherLabel)
                            .addComponent(publisherComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pubdateLabel)
                            .addComponent(pubdateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(notesScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Information", Tab1Panel);

        jLabel1.setText("Search String");

        jTextField1.setText("jTextField1");

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "null", "Title 2", "Title 3"
            }
        ));
        jScrollPane3.setViewportView(jTable2);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 755, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 681, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Search", jPanel1);

        jSplitPane2.setBottomComponent(jTabbedPane1);
        jTabbedPane1.getAccessibleContext().setAccessibleName("          Rolodex       ");

        jSplitPane1.setRightComponent(jSplitPane2);

        jScrollPane1.setViewportView(jTree1);

        jSplitPane1.setLeftComponent(jScrollPane1);

        jProgressBar1.setBackground(java.awt.SystemColor.inactiveCaption);

        MsgArea.setEditable(false);
        MsgArea.setFont(new java.awt.Font("Georgia", 1, 11));
        MsgArea.setText("MsgArea");
        MsgArea.setFocusable(false);
        MsgArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MsgAreaActionPerformed(evt);
            }
        });

        jMenuBar1.setFont(new java.awt.Font("Georgia", 1, 11));

        MenuFile.setText("File");
        MenuFile.setFont(new java.awt.Font("Georgia", 1, 11));

        SetCatRoot.setText("Select Library Root Directory");
        SetCatRoot.setName(""); // NOI18N
        SetCatRoot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SetCatRootActionPerformed(evt);
            }
        });
        MenuFile.add(SetCatRoot);

        Exit.setText("Exit");
        Exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExitActionPerformed(evt);
            }
        });
        MenuFile.add(Exit);

        jMenuBar1.add(MenuFile);

        MenuOptions.setText("Options");
        MenuOptions.setFont(new java.awt.Font("Georgia", 1, 11));

        ColorMenu.setText("Color Scheme Editor");
        ColorMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ColorMenuActionPerformed(evt);
            }
        });
        MenuOptions.add(ColorMenu);

        jMenuBar1.add(MenuOptions);

        MenuDatabase.setText("Database");
        MenuDatabase.setFont(new java.awt.Font("Georgia", 1, 11));

        dbClean.setText("Clean");
        dbClean.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dbCleanActionPerformed(evt);
            }
        });
        MenuDatabase.add(dbClean);

        dbConnect.setText("Connect");
        dbConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dbConnectActionPerformed(evt);
            }
        });
        MenuDatabase.add(dbConnect);

        dbDisconnect.setText("Disconnect");
        dbDisconnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dbDisconnectActionPerformed(evt);
            }
        });
        MenuDatabase.add(dbDisconnect);

        jMenuBar1.add(MenuDatabase);

        MenuHelp.setText("Help");
        MenuHelp.setFont(new java.awt.Font("Georgia", 1, 11));
        MenuHelp.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);

        Debug.setText("Debug");
        Debug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DebugActionPerformed(evt);
            }
        });
        MenuHelp.add(Debug);

        jMenuBar1.add(MenuHelp);
        MenuHelp.getAccessibleContext().setAccessibleName("MenuHelp");

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(MsgArea, javax.swing.GroupLayout.DEFAULT_SIZE, 691, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 888, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 682, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(MsgArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void MsgAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MsgAreaActionPerformed

    }//GEN-LAST:event_MsgAreaActionPerformed
    private void DebugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DebugActionPerformed

    }//GEN-LAST:event_DebugActionPerformed
    private void dbConnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dbConnectActionPerformed
	main.getDB().DBconnect();
    }//GEN-LAST:event_dbConnectActionPerformed
    private void dbDisconnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dbDisconnectActionPerformed
	main.getDB().DBdisconnect();
    }//GEN-LAST:event_dbDisconnectActionPerformed
    private void SetCatRootActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SetCatRootActionPerformed
	jFileChooserSelect();
    }//GEN-LAST:event_SetCatRootActionPerformed
    private void ExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExitActionPerformed
	main.exitApp();
    }//GEN-LAST:event_ExitActionPerformed
    private void dbCleanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dbCleanActionPerformed
	main.getDB().DBclean();
    }//GEN-LAST:event_dbCleanActionPerformed
    private void ColorMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ColorMenuActionPerformed
	jColorChooser("Set Color Scheme");
}//GEN-LAST:event_ColorMenuActionPerformed
    private void imagePanelActionPerformed(java.awt.event.ActionEvent evt){
	if(evt.getActionCommand().equals("Copy")){
                Transferable xfer = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
    
        try {
            if (xfer != null && xfer.isDataFlavorSupported(DataFlavor.imageFlavor)) {
                Image image = (Image)xfer.getTransferData(DataFlavor.imageFlavor);
//TODO Code to place image in imagePanel
	    }
        } catch (UnsupportedFlavorException ex) {
        } catch (IOException ex) {}
	} else if(evt.getActionCommand().equals("Paste")){
	    Transferable xfer = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
	    try {
                if(xfer.isDataFlavorSupported(DataFlavor.imageFlavor)) {
		    Image image = (Image)xfer.getTransferData(DataFlavor.imageFlavor);
		    setImagePanel(image);
		}
	    } catch(UnsupportedFlavorException ex) {} 
              catch(IOException ex) {}
	}
    }
    private void jTree1ActionPerformed(java.awt.event.ActionEvent evt) {
	if(evt.getActionCommand().equals("Open")) {
	    jTree1DoubleClick(jTree1.getSelectionPath(), true);
	} else if(evt.getActionCommand().equals("Refresh")) {
            Runnable setGUIFile = new Runnable() {
            @Override
                public void run() {
                    File elib_dir = new File(configs.get("ROOTDIR").toString());
                    refreshTree();
                    setMsgArea("Background Process: Updating DB with "+main.igetRecursiveFileCount(elib_dir)+" files.");
                    addTreeToDB(elib_dir, 0);
                    setMsgArea(main.getDefaultMsgText());                    
                }
            };
            Thread thread = new Thread(setGUIFile);
            thread.setPriority(Thread.NORM_PRIORITY - 1);
            thread.start();
	} else if(evt.getActionCommand().equals("Copy")) {
            fromFile = main.igetFileFromPath(getFileNameFromTreePath(jTree1.getSelectionPath()));
	    delbool = false;
	} else if(evt.getActionCommand().equals("Cut")) {
            fromFile = main.igetFileFromPath(getFileNameFromTreePath(jTree1.getSelectionPath()));
	    delbool = true;
	} else if(evt.getActionCommand().equals("Paste")) {
            String fromfilename = fromFile.getName();
            String toPath = getFileNameFromTreePath(jTree1.getSelectionPath()).replace(main.igetFileFromPath(getFileNameFromTreePath(jTree1.getSelectionPath())).getName(), "");
            File toFile = new File(toPath + fromfilename);
 	    if(fromFile.exists() && !toFile.exists()) {
		try {
                    main.icopyFile(fromFile, toFile);
		    if(! delbool) {
			String sql = "INSERT INTO ELIBRARY.BOOKS";
                        // TODO Copy data from existing entry and add to new row
			refreshTree();
		    }
		} catch (FileNotFoundException ex) {
		    Logger.getLogger(GUI_Main.class.getName()).log(Level.SEVERE, null, ex);
		}
            }
	    if(delbool) {
                if(main.ideleteFile(fromFile)) {
                    String sql = "UPDATE ELIBRARY.BOOKS "+
                    "SET FILE = '"+toFile+"' "+
                    "WHERE FILE = '"+fromFile+"'";
                    main.getDB().DBstatement(sql);
                    refreshTree();
		}
	    }
	    delbool = false;
	    fromFile = null;
	} else if(evt.getActionCommand().equals("MkDir")) {
	    //TODO Dialog box to get dir name and method to create directory.
	} else if(evt.getActionCommand().equals("Rename")) {
	    File file = main.igetFileFromPath(getFileNameFromTreePath(jTree1.getSelectionPath()));
            InputBox rBox = new InputBox();
	    rBox.setVisible(true);
	    rBox.setName(file.getName());
	    rBox.setTitle("Rename File");
	    rBox.setLocation(jTree1.getMousePosition());
//            if (dir_select.getShowOpenDialog() == dir_select.getApproveOption()){
System.out.println(rBox);

	    String desName = rBox.getName();
	    String desPath = file.getPath();
	    String desFile = desPath+File.pathSeparator+desName;
	    File desfile = new File(desFile);
	    file.renameTo(desfile);
            String sql = "UPDATE ELIBRARY.BOOKS "+
            "SET FILE = '"+desfile.getAbsolutePath()+"' "+
            "WHERE FILE = '"+desfile.getAbsolutePath()+"'";
            main.getDB().DBstatement(sql);	   
            File dir = new File(configs.get("ROOTDIR").toString());
            File[] files = dir.listFiles();
            createjTreeModel(files);	    
        } else if(evt.getActionCommand().equals("Delete")) {
	    File file = main.igetFileFromPath(getFileNameFromTreePath(jTree1.getSelectionPath()));
	    boolean status = main.ideleteFile(file);
	    if(status) {
            status = main.getDB().DBstatement("delete from ELIBRARY.BOOKS where FILE = '"+file.getAbsolutePath()+"'");
            refreshTree();
	    } else {
		setMsgArea("Delete "+file.getName()+" failed.");
	    }
	}
    }
    private void jTable1ActionPerformed(java.awt.event.ActionEvent evt) {
	if(!jTable1.getModel().getValueAt(jTable1.getSelectedRow(), 4).equals("null")) {
            if(evt.getActionCommand().equals("Open")) {
                int[] selRows = jTable1.getSelectedRows();
                for (int i=0; i < selRows.length; i++) {
                    String filePath = jTable1.getModel().getValueAt(selRows[i], 4).toString();
                    jTable1DoubleClick(filePath, true);
                }
            } else if(evt.getActionCommand().equals("Refresh")) {
                File selFile = new File(jTable1.getModel().getValueAt(jTable1.getSelectedRow(), 4).toString());
                File dir = new File(selFile.getParent());
                createjTable(dir);
                File rootdir = new File(configs.get("ROOTDIR").toString());
                int rootdircount = main.igetRecursiveFileCount(rootdir);
                int dbcount = main.getDB().SQL_selectCount("ELIBRARY.BOOKS");
                if(dbcount != rootdircount) {
                    Runnable setGUIFile = new Runnable() {
                    @Override
                        public void run() {
                            File rootdir = new File(configs.get("ROOTDIR").toString());
                            setMsgArea("Updating DB with "+main.igetRecursiveFileCount(rootdir)+" files.");
                            addTreeToDB(rootdir, 0);                   
                        }
                    };	
                    Thread thread = new Thread(setGUIFile);
                    thread.setPriority(Thread.NORM_PRIORITY - 1);
                    thread.start();
                }
                setMsgArea(main.getDefaultMsgText()); 
            } else if(evt.getActionCommand().equals("Copy")) {
                int[] selRows = jTable1.getSelectedRows();
                fromFiles = new String[selRows.length];
                for (int i=0; i < selRows.length; i++) {
                    fromFiles[i] = jTable1.getModel().getValueAt(selRows[i], 4).toString();
                }
                delbool = false;
                Clipboard clipboard = this.getToolkit().getSystemClipboard();
                Transferable xfer = new Transferable() {
		    @Override
                    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException ,IOException {
                        if (flavor.equals(DataFlavor.javaFileListFlavor)) {
                            List <String>  fileList = new ArrayList <String>() ;
                            for (int i=0;i<fromFiles.length;i++) {
                                fileList.add(fromFiles[i]);
                            }
                            return fileList;
                        }
                        throw new UnsupportedFlavorException(flavor);
                    }
		    @Override
                    public DataFlavor[] getTransferDataFlavors() {
                        return new DataFlavor[] { DataFlavor.javaFileListFlavor };
                    }
		    @Override
                    public boolean isDataFlavorSupported(DataFlavor flavor) {
                        return flavor.equals(DataFlavor.javaFileListFlavor);
                    }
                };
                clipboard.setContents(xfer, null);
            } else if(evt.getActionCommand().equals("Cut")) {
                int[] selRows = jTable1.getSelectedRows();
                fromFiles = new String[selRows.length];
                for (int i=0; i < selRows.length; i++) {
                    fromFiles[i] = jTable1.getModel().getValueAt(selRows[i], 4).toString();
                }
                delbool = true;
                Clipboard clipboard = this.getToolkit().getSystemClipboard();
                Transferable xfer = new Transferable() {
		    @Override
                    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException ,IOException {
                        if (flavor.equals(DataFlavor.javaFileListFlavor)) {
                            List <String>  fileList = new ArrayList <String>() ;
                            for (int i=0;i<fromFiles.length;i++) {
                                fileList.add(fromFiles[i]);
                            }
                            return fileList;
                        }
                        throw new UnsupportedFlavorException(flavor);
                    }
		    @Override
                    public DataFlavor[] getTransferDataFlavors() {
                        return new DataFlavor[] { DataFlavor.javaFileListFlavor };
                    }
		    @Override
                    public boolean isDataFlavorSupported(DataFlavor flavor) {
                        return flavor.equals(DataFlavor.javaFileListFlavor);
                    }
                };
                clipboard.setContents(xfer, null);
            } else if(evt.getActionCommand().equals("Paste")) {
                // Get FileList from Clipboard
                Clipboard clipboard = this.getToolkit().getSystemClipboard();
                Transferable xfer = clipboard.getContents(this);
                try { 
                    if (xfer.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                        List fileList = (List) xfer.getTransferData(DataFlavor.javaFileListFlavor);
                        fromFiles = new String[fileList.size()];
                        for (int i=0;i<fileList.size();i++){
                            fromFiles[i] = fileList.get(i).toString();
                        }
                    }
            }
            catch (Exception ex) {
                Logger.getLogger(GUI_Main.class.getName()).log(Level.SEVERE, null, ex);
	    }
	    // Copy Files
        Runnable copyFiles = new Runnable() {
		    @Override
	    public void run() {
            // Get Total File Size to be Pasted
                float pastesize = 0;
                for (int i=0;i<fromFiles.length;i++){
                    File file = new File(fromFiles[i]);
                    pastesize = (pastesize + main.igetFileSize(file));
                }
                int barmax = (int) pastesize;
		setProgressBarTasks(barmax);
                File selDir = new File(getFileNameFromTreePath(jTree1.getSelectionPath()));
		int barprogress = 0;
                for (int i=0;i<fromFiles.length;i++){
                    fromFile = new File(fromFiles[i]);
		    int filesize = (int) main.igetFileSize(fromFile);
		    barprogress = (barprogress + filesize);
                    setMsgArea(fromFile.getName());
		    String fromfilename = fromFile.getName();
                    File toFile = new File(selDir+File.separator+ fromfilename);
                    if(fromFile.exists() && !toFile.exists()) {
                        try {
                            main.icopyFile(fromFile, toFile);
			    createjTable(selDir);
                            if(! delbool) {
                                String sql = "INSERT INTO ELIBRARY.BOOKS";
                                // TODO Copy data from existing entry and add to new row
                            }
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(GUI_Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    setProgressBarTask(barprogress);
                    if(delbool) {
                        if(main.ideleteFile(fromFile)) {
                            String sql = "UPDATE ELIBRARY.BOOKS "+
                            "SET FILE = '"+toFile+"' "+
                            "WHERE FILE = '"+fromFile+"'";
                            main.getDB().DBstatement(sql);
                        }
                    }
                }
		setProgressBarTask(barmax);
		setMsgArea(main.getDefaultMsgText());
	    }
	};
	Thread thread = new Thread(copyFiles);
	thread.setPriority(Thread.NORM_PRIORITY - 1);
	thread.start();
        File selDir = new File(getFileNameFromTreePath(jTree1.getSelectionPath()));
        createjTable(selDir);
	} else if (evt.getActionCommand().equals("Delete")) {
	    int[] selRows = jTable1.getSelectedRows();
        File aFile = new File(jTable1.getModel().getValueAt(jTable1.getSelectedRow(), 4).toString());
	    File parent = aFile.getParentFile();
        for (int i=0;i<selRows.length;i++){
            File delFile = new File(jTable1.getModel().getValueAt(selRows[i], 4).toString());
            main.ideleteFile(delFile);
	    }
	    createjTable(parent);
//	    refreshTree();
	} else if (evt.getActionCommand().equals("Rename")) {
	    
	}
	}
    }
    
    public static void main(String args[]) {
	java.awt.EventQueue.invokeLater(new Runnable() {
	    @Override
	    public void run() {
                //TODO ADD Rename File Code
	    }
	});
    }
    
    @Override
    public void actionPerformed( java.awt.event.ActionEvent evt ) {

    }
    void addDbFILE(File file) {
	if(file.getAbsoluteFile().toString().contains("'") || file.getAbsoluteFile().toString().contains(",")) {
            if(file.getAbsoluteFile().toString().contains("'")) {
        	    File newfile = file;
                    newfile.getAbsoluteFile().toString().replace("'", "");
                    file = newfile;
	    }
            if(file.getAbsoluteFile().toString().contains(",")) {
        	    File newfile = file;
                    newfile.getAbsoluteFile().toString().replace(",", ".");
                    file = newfile;
	    }	    
	    file = main.irenameFile(file);
	}
        String query = "ELIBRARY.BOOKS WHERE FILE = '"+file.getAbsoluteFile().toString()+"'";
	if(!main.getDB().SQL_exists("FILE", query)) {
            int maxid = main.getDB().SQL_selectMaxforTable("BOOK_ID", "ELIBRARY.BOOKS") + 1;
	    String sql = "INSERT INTO ELIBRARY.BOOKS VALUES("+
                maxid+","+"null,"+"null,"+"null,'"+file.getAbsoluteFile().toString()+"',"+
                "null,"+"null,"+"null,"+"null,"+"null,"+
		"null,"+"null,"+"null,"+"null,"+"null,"+
		"null,"+"null,"+"null)";
            main.getDB().DBstatement(sql);
	}	
    }    
    public int addTreeToDB(File file, int t) {
        if(file.getAbsolutePath().equals(configs.get("ROOTDIR").toString())) {
            int max = main.igetRecursiveFileCount(new File(file.getAbsolutePath()));
            setProgressBarTasks(max);
	}
	if (file.isDirectory()) {
	    String[] children = file.list();
            for (int i=0;i<children.length;i++) {
                t=addTreeToDB(new File(file, children[i]), t);
	    }
	} else if (file.isFile()) {
	    addDbFILE(file);
	    if (file.getAbsolutePath().endsWith(".pdf")) {
//Process takes too long..
//                pdfGetNumOfPages(file.getAbsolutePath());
	    }
	    t++;
	    setProgressBarTask(t);
	}
	return(t);
	
    }
    public void createimagePanelPopupMenu() {
	String[] imagePanelItemNames = {"Copy", "Paste"};
	JMenuItem[] imagePanelMenuItems = new JMenuItem[imagePanelItemNames.length];
        for (int i=0;i < imagePanelMenuItems.length;i++){
                imagePanelMenuItems[i] = new JMenuItem(imagePanelItemNames[i]);
	}
	// Customize Items
	Color[] pcolors = main.getColors();
	Color itemBGColor = pcolors[17];
	Color itemFGColor = pcolors[18];
        for (int i=0;i < imagePanelMenuItems.length;i++){
	    jPopupMenuimagePanel.add(imagePanelMenuItems[i]);
	    imagePanelMenuItems[i].setBackground(itemBGColor);
	    imagePanelMenuItems[i].setForeground(itemFGColor);
	}
	// Action and mouse listener support
	jPopupMenuimagePanel = new JPopupMenu();
        for (int i=0;i < imagePanelItemNames.length;i++){
            if(!imagePanelItemNames[i].equals("_")){
                jPopupMenuimagePanel.add(imagePanelMenuItems[i]);
	    } else {
		jPopupMenuimagePanel.addSeparator();
	    }
	}
        jPopupMenuimagePanel.setBackground(itemBGColor);
	jPopupMenuimagePanel.setForeground(itemFGColor);
	jPopupMenuimagePanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
	jPopupMenuimagePanel.setFont(font);
	imagePanel.add(jPopupMenuimagePanel);
	ActionListener jImagePanelPopupMenuActionPerformed = new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent evt) {
		imagePanelActionPerformed(evt);
	    }
	};	
        imagePanel.setComponentPopupMenu(jPopupMenuimagePanel);
	enableEvents(AWTEvent.MOUSE_EVENT_MASK);
	for (int i=0;i < imagePanelMenuItems.length;i++){
	    imagePanelMenuItems[i].addActionListener(jImagePanelPopupMenuActionPerformed);
	}
    }
    public void createjTreePopupMenu() {
	String[] treeItemNames = {"Open", "Refresh", "_", "Rename", "MkDir", "Delete"};
	JMenuItem[] treeMenuItems = new JMenuItem[treeItemNames.length];
        for (int i=0;i < treeItemNames.length;i++){
                treeMenuItems[i] = new JMenuItem(treeItemNames[i]);
	}
	// Customize Items
	Color[] pcolors = main.getColors();
	Color itemBGColor = pcolors[17];
	Color itemFGColor = pcolors[18];
        for (int i=0;i < treeMenuItems.length;i++){
	    jPopupMenuTree.add(treeMenuItems[i]);
	    treeMenuItems[i].setBackground(itemBGColor);
	    treeMenuItems[i].setForeground(itemFGColor);
	}
	// Action and mouse listener support
	jPopupMenuTree = new JPopupMenu();
        for (int i=0;i < treeItemNames.length;i++){
            if(!treeItemNames[i].equals("_")){
                jPopupMenuTree.add(treeMenuItems[i]);
	    } else {
		jPopupMenuTree.addSeparator();
	    }
	}
        jPopupMenuTree.setBackground(itemBGColor);
        jPopupMenuTree.setForeground(itemFGColor);
	jPopupMenuTree.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
	jPopupMenuTree.setFont(font);
	jTree1.add(jPopupMenuTree);
	ActionListener jTree1PopupMenuActionPerformed = new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent evt) {
		jTree1ActionPerformed(evt);
	    }
	};	
        jTree1.setComponentPopupMenu(jPopupMenuTree);
	enableEvents(AWTEvent.MOUSE_EVENT_MASK);
	for (int i=0;i < treeMenuItems.length;i++){
	    treeMenuItems[i].addActionListener(jTree1PopupMenuActionPerformed);
	}
    }
    public void createjTablePopupMenu() {
	String[] tableItemNames = {"Open", "Refresh", "_", "Copy", "Cut", "Paste", "_", "Delete", "Rename"}; 
	JMenuItem[] tableMenuItems = new JMenuItem[tableItemNames.length];
        for (int i=0;i < tableItemNames.length;i++){
	    tableMenuItems[i] = new JMenuItem(tableItemNames[i]);
	}
	// Customize Items
	Color[] pcolors = main.getColors();
	Color itemBGColor = pcolors[17];
	Color itemFGColor = pcolors[18];
        for (int i=0;i < tableMenuItems.length;i++){
	    jPopupMenuTable.add(tableMenuItems[i]);
	    tableMenuItems[i].setBackground(itemBGColor);
	    tableMenuItems[i].setForeground(itemFGColor);
	}
	// Action and mouse listener support
	jPopupMenuTable = new JPopupMenu();
        for (int i=0;i < tableMenuItems.length;i++){
            if(!tableItemNames[i].equals("_")){
                jPopupMenuTable.add(tableMenuItems[i]);
	    } else {
		jPopupMenuTable.addSeparator();
	    }
	}	
    jPopupMenuTable.setBackground(itemBGColor);
	jPopupMenuTable.setForeground(itemFGColor);
	jPopupMenuTable.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
	jPopupMenuTable.setFont(font);
	jTable1.add(jPopupMenuTable);
	ActionListener jTable1PopupMenuAL = new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent evt) {
		jTable1ActionPerformed(evt);
	    }
	};	
        jTable1.setComponentPopupMenu(jPopupMenuTable);
	enableEvents(AWTEvent.MOUSE_EVENT_MASK);
	for (int i=0;i < tableMenuItems.length;i++){
	    tableMenuItems[i].addActionListener(jTable1PopupMenuAL);
	}
	delbool = false;
        fromFile = null;	    
    }
    public void createjTable(File dir) {
       DefaultTableModel model = new DefaultTableModel(new String[]{" ","Name","Size","Modified On", "Path"}, 0) {
	    @Override
            public Class getColumnClass(int columnIndex) {
                return columnIndex==0? Icon.class : String.class;
            }
	    @Override
            public boolean isCellEditable(int rowIndex, int columnIndex){
                return false;
            }
        };
	CustomTableDataRenderer custom = new CustomTableDataRenderer();
	CustomHeaderRenderer custheader = new CustomHeaderRenderer();
        // Set or Restore Column Sizes
	int[] colsizes = new int[]{16,720,60,200,0};
    jTable1.setName("Table1");
    if(jTable1.getColumnModel().getColumn(0).getMaxWidth() != 16){
        String cols = configs.get("TABLE_COLS").toString();
        String[] colw = cols.split("\\.");
        for (int i=0;i < colw.length;i++) {
            if(i == 0) {
                colsizes[i] = 16;
            } else {
                colsizes[i] = Integer.valueOf(colw[i]);
            }
        }
	} else {
            colsizes = new int[]{16,jTable1.getColumnModel().getColumn(1).getWidth(),jTable1.getColumnModel().getColumn(2).getWidth(),jTable1.getColumnModel().getColumn(3).getWidth(),0};
    }
	// Build Table Data
    String[] dirlist = dir.list();
	File file;
    // Directories first
	for (int i=0;i < dirlist.length;i++) {
	    file = new File(dir+File.separator+dirlist[i]);
	    if(file.isDirectory()) {
                long modtime = file.lastModified();
                Date mtime = new Date(modtime);
                String filetime = DateFormat.getDateInstance(DateFormat.MEDIUM).format(mtime)+" "+DateFormat.getTimeInstance(DateFormat.SHORT).format(mtime);
                DecimalFormat formatter = new DecimalFormat( "#,###,###,##0.00" );
                String sizevalue = String.valueOf(formatter.format(main.igetFileSize(file)));
                model.addRow(new Object[]{
                    new ImageIcon(main.getFileURL(file)),
                    new String(" "+file.getName()),
                    sizevalue+" MB",
                    String.valueOf(filetime),
		    new String(dir+File.separator+dirlist[i])
                });		
	    }
	}
	//Files second
	for (int i=0;i < dirlist.length;i++) {
	    file = new File(dir+File.separator+dirlist[i]);
            if(file.isFile()) {
                long modtime = file.lastModified();
                Date mtime = new Date(modtime);
                String filetime = DateFormat.getDateInstance(DateFormat.MEDIUM).format(mtime)+" "+DateFormat.getTimeInstance(DateFormat.SHORT).format(mtime);
                DecimalFormat formatter = new DecimalFormat( "#,###,###,##0.00" );
                String sizevalue = String.valueOf(formatter.format(main.igetFileSize(file)));
                model.addRow(new Object[]{
                    new ImageIcon(main.getFileURL(file)),
                    new String(" "+file.getName()),
                    sizevalue+" MB",
                    String.valueOf(filetime),
                    new String(dir+File.separator+dirlist[i])
                });
	    }
	}
	//Add White Space if needed
	if(dirlist.length < 50) {
	    String treeDir = "null";
	    if(!jTree1.isSelectionEmpty()) {
		treeDir = getFileNameFromTreePath(jTree1.getSelectionPath());
	    }
	    int whiteloop = ( 50 - dirlist.length);
            for (int i=0;i<whiteloop;i++){
                model.addRow(new Object[]{
                    null,
                    null,
                    null,
                    null,
                    treeDir
                });
	    }
	}
        jTable1.getTableHeader().setDefaultRenderer(custheader);
	jTable1.setModel(model);
        jTable1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
       
	for (int i=0;i < colsizes.length;i++) {
	    int colwidth = colsizes[i];
		jTable1.getColumnModel().getColumn(i).setPreferredWidth(colwidth);
                jTable1.getColumnModel().getColumn(i).setResizable(true);
                jTable1.getColumnModel().getColumn(i).setWidth(colwidth);
		jTable1.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
		if(i == 0) {
                jTable1.getColumnModel().getColumn(i).setMinWidth(colwidth);
                jTable1.getColumnModel().getColumn(i).setMaxWidth(colwidth);
                jTable1.getColumnModel().getColumn(i).setResizable(false);
	    } else if (i == 1) {
		jTable1.getColumnModel().getColumn(i).setCellRenderer(custom);                
	    } else if (i == 2) {
		jTable1.getColumnModel().getColumn(i).setCellRenderer(custom);
		jTable1.getColumnModel().getColumn(i).setMinWidth(60);
	    } else if (i == 3) {
		jTable1.getColumnModel().getColumn(i).setCellRenderer(custom);
		jTable1.getColumnModel().getColumn(i).setMinWidth(120);
	    }
                else if (i == 4) {
		jTable1.getColumnModel().getColumn(i).setMinWidth(0);
		jTable1.getColumnModel().getColumn(i).setMaxWidth(0);
		jTable1.getColumnModel().getColumn(i).setWidth(0);
	    }
	} 
    jTable1.setRowSelectionAllowed(true);
    jTable1.setColumnSelectionAllowed(false);
    jTable1.setAutoCreateRowSorter(true);
	jScrollPane2.getVerticalScrollBar().setValue(0);
    }
    public void createjTreeModel(File[] files) {
	CustomTreeCellRenderer custom = new CustomTreeCellRenderer();
	jTree1.setCellRenderer(custom);
       
	DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("eLibrary");
	for (int i=0; i < files.length; i++) {
	    DefaultMutableTreeNode root = new DefaultMutableTreeNode(files[i].getName());
	    rootNode.add(root);
	    if(files[i].isDirectory()) {
                createjTreeNode(jTree1, root, files[i]);
	    }
	}
	ToolTipManager.sharedInstance().registerComponent(jTree1);
	ToolTipManager.sharedInstance().setInitialDelay(1500);
	ToolTipManager.sharedInstance().setDismissDelay(30000);
	ToolTipManager.sharedInstance().setReshowDelay(15000);
	DefaultTreeModel treeModel = new DefaultTreeModel(rootNode);
	jTree1.setLargeModel(true);
	jTree1.setBackground(colors[10]);
	jTree1.setModel(treeModel);
	jTree1.setFont(font);
    }
    public void createjTreeNode(JTree tree,DefaultMutableTreeNode node,File file) {
        if(file.isDirectory()) {
            CustomTreeCellRenderer custom = new CustomTreeCellRenderer();
	    tree.setCellRenderer(custom);
	    File[] files = file.listFiles();
	    for (int i=0;i < files.length;i++) {
		if(files[i].isDirectory()) { 
                    DefaultMutableTreeNode newnode = new DefaultMutableTreeNode(files[i].getName());
		    node.add(newnode);
		    createjTreeNode(tree, newnode, files[i]);
		}
	    }
	}
    }
    void enableMenus(boolean b) {
	MenuFile.setEnabled(b);
	MenuDatabase.setEnabled(b);
	MenuOptions.setEnabled(b);
	MenuHelp.setEnabled(b);
    }
    void expandJTree(int i) {
	jTree1.expandRow(i);
    }
    public int getColumnWidth(int i) {
	return(jTable1.getColumnModel().getColumn(i).getWidth());
    }
    public void getConfigs() {
        configs = (Map) main.getDB().DBselect("select * from ELIBRARY.CONFIGS").get(0);
    }
    public int getSplitPaneDeviderInteger() {
	return(jSplitPane1.getDividerLocation());
    }
    public int getSplitPane2DeviderInteger() {
	return(jSplitPane2.getDividerLocation());
    }
    public boolean getExpansionState(JTree tree, int row){
        boolean status = false;
	TreePath rowPath = tree.getPathForRow(row);
        int rowCount = tree.getRowCount();
        for(int i=row; i<rowCount; i++){
            TreePath path = tree.getPathForRow(i);
            if(i==row || isDescendant(path, rowPath)){
                if(tree.isExpanded(i)) {
                    status = true;
                    break;
		}
	    }else
                status = false;
	}
        return status;
    }
    public String getFileNameFromTreePath(TreePath selPath) {
        String rootPath = configs.get("ROOTDIR").toString();
        String prePath = selPath.toString().replaceAll("]", "");
        String[] subpath = prePath.split(", ");
        String path = rootPath+System.getProperty("file.separator");
        for (int i = 0; i < selPath.getPathCount(); i++) {
            if (i != 0) {
                if (i < (selPath.getPathCount() - 1)) {
                    path = path + subpath[i] + System.getProperty("file.separator");
                } else {
                    path = path + subpath[i];
                }
            }
        }
	return(path);
    }
    public String getTreePathFromFileName(String fileName) {
	File selFile = new File(fileName);
    String rootDir = configs.get("ROOTDIR").toString();
	String treePath = "[";
	File selParent;
	if(selFile.isFile()){
            selParent = selFile.getParentFile();
	} else {
	    selParent = selFile;
	}
        String subPaths = selParent.getAbsolutePath().replace(rootDir, "");
        String[] paths = subPaths.split("\\"+File.separator);
        for (int i=0;i<paths.length;i++){
            String addPath;
            if(i == 0) {
                addPath = "eLibrary";
            } else {
                addPath = ", "+paths[i];
            }
            treePath += addPath;
        }
        treePath += "]";
	return(treePath);
    }
    File getFileFromTreePath(TreePath selPath) {
	File file = new File(getFileNameFromTreePath(selPath));
	return(file);
    }
    public Icon getSystemFileIcon(File file) {
        Icon icon = null;
        if(file.exists()) {
            icon = main.getFileChooser().getFileSystemView().getSystemIcon(file);	
        } else {
            icon = (Icon) new ImageIcon(main.getURL("FileMissing.png"));
        }
        return(icon);
    }    
    public boolean isDescendant(TreePath path1, TreePath path2){
        int count1 = path1.getPathCount();
        int count2 = path2.getPathCount();
        if(count1<=count2)
            return false;
        while(count1!=count2){
            path1 = path1.getParentPath();
            count1--;
        }
        return path1.equals(path2);
    }
    public void jColorChooser(final String title) {
	final Color[] ccColors = main.getColors();
	final Color color = ccColors[2];
	Runnable setColor = new Runnable() {
	    @Override
            public void run() {
                final Color[] originals = main.getColors();
		final Color[] origColors = new Color[originals.length];
		for (int i=0;i<originals.length;i++){
		    origColors[i] = originals[i];
		}
                final JFrame ccFrame = new JFrame(title);
                final JColorChooser cc = new JColorChooser(color);
                final JButton ButtonDiscard = new javax.swing.JButton("Discard");
                final JButton ButtonApply = new javax.swing.JButton("Apply");
                final JButton ButtonSave = new javax.swing.JButton("Save");
		final JComboBox ComboBoxElements = new javax.swing.JComboBox();
		ccFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		javax.swing.GroupLayout ccFrameLayout = new javax.swing.GroupLayout(ccFrame.getContentPane());
		String[] cbElements = new String[] {
		    "Table Font Color",
		    "Table Font Selected Color",
                    "Table Primary Color",
		    "Table Alternate Color",
		    "Table Primary Selected Color",
		    "Table Alternate Selected Color",
		    "Table Header Color",
		    "Table Header Font Color",
		    "Tab1 Panel Foreground Color",
		    "Tab1 Panel Background Color",
		    "Tree Panel Background Color",
		    "Tree Panel Foreground Color",
		    "Tree Panel Selected Color",
		    "Menu Panel Background Color",
		    "Menu Panel Foreground Color",
		    "Split Pane Background Color",
		    "Split Pane Foreground Color",
		    "Popup Menu Background Color",
		    "Popup Menu Foreground Color"
                };
                ComboBoxElements.setModel(new javax.swing.DefaultComboBoxModel(cbElements));
		ccFrame.getContentPane().setLayout(ccFrameLayout);
                ccFrameLayout.setHorizontalGroup(
                    ccFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGap(0, 500, Short.MAX_VALUE)
                );
                ccFrameLayout.setVerticalGroup(
                    ccFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGap(0, 400, Short.MAX_VALUE)
                
                );
		javax.swing.GroupLayout cclayout = new javax.swing.GroupLayout(ccFrame.getContentPane());
                ccFrame.getContentPane().setLayout(cclayout);
		cclayout.setHorizontalGroup(
                    cclayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		    .addGroup(cclayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(cclayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cc, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cclayout.createSequentialGroup()
                                .addComponent(ComboBoxElements, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(18, 18, 18)			    
                                .addComponent(ButtonApply, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ButtonSave, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ButtonDiscard)))
                        .addContainerGap())
                );
                cclayout.setVerticalGroup(
                    cclayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cclayout.createSequentialGroup()
                        .addComponent(cc, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(cclayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ButtonDiscard)
                            .addComponent(ButtonSave)
                            .addComponent(ButtonApply)
                            .addComponent(ComboBoxElements, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
			.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
                ccFrame.pack();
		final int locx = (jTabbedPane1.getLocationOnScreen().x + 100);
		final int locy = (jTabbedPane1.getLocationOnScreen().y - 40);
                ccFrame.setLocation(locx,locy);
		ComboBoxElements.setSelectedIndex(2);
                ccFrame.setVisible(true);
                MouseListener ccMouseListener = new MouseAdapter() {
		    @Override
                    public void mousePressed(MouseEvent mc) {
			if(mc.getButton() == MouseEvent.BUTTON1) {
                            if(mc.getClickCount() == 1) {
                                int c = ComboBoxElements.getSelectedIndex();
				if(mc.getSource().equals(ButtonApply)) {
                                    ccColors[c] = cc.getColor();
                                    main.setColors(ccColors);
				    actionsMouseButton1(c);
                                } else if (mc.getSource().equals(ButtonSave)) {
                                    ccColors[c] = cc.getColor();
                                    main.setColors(ccColors);
                                    actionsMouseButton1(c);
				    ccFrame.dispose();
                                } else if (mc.getSource().equals(ButtonDiscard)){
                                    main.setColors(origColors);
                                    actionsMouseButton1(c);
				    ccFrame.dispose();
				}
                            }
                        }
                    }
		    public void actionsMouseButton1(int c) {
                File tableDir = new File(configs.get("ROOTDIR").toString());
                if (c<=7) {createjTable(tableDir);}
                else if (c==8) {setTabPane1jPanelForeGroundColor(ccColors[c]);}
                else if (c==9) {setTabPane1jPanelBackGroundColor(ccColors[c]);}
                else if (c<=10 && c<=12) {setjTree1BackGroundColor();}
                else if (c==13||c==14) {setMenuPanelColors();}
                else if (c==15||c==16) {setSplitPaneColor();}
                else if (c==17||c==18) {setPopupMenuColors();}
		    }
                };
		ButtonApply.addMouseListener(ccMouseListener);
		ButtonSave.addMouseListener(ccMouseListener);
                ButtonDiscard.addMouseListener(ccMouseListener);
	    }
	};
	Thread thread = new Thread(setColor);
	thread.setPriority(Thread.NORM_PRIORITY - 1);
        thread.start();
    }
    public void jFileChooserSelect() {
        Runnable setGUIFile = new Runnable() {
	    @Override
	    public void run() {
                GUI_FileSelect dir_select = new GUI_FileSelect();
                dir_select.SetSelectMode(1); // Directories Only
                dir_select.SetWinTitle("Select path to your eLibrary books.");
                if (dir_select.getShowOpenDialog() == dir_select.getApproveOption()){
                            File elib_dir = dir_select.GetSelection();
                            String sql = "UPDATE ELIBRARY.CONFIGS "+
                            "SET ROOTDIR = '"+elib_dir+"' "+
                            "WHERE ID = 1";
                            main.getDB().DBstatement(sql);
                            File[] files = elib_dir.listFiles();
                            createjTreeModel(files);
			    setjTreeSelect(0);
			    createjTable(elib_dir);
                getConfigs();
			    setMsgArea("Updating DB with "+main.igetRecursiveFileCount(elib_dir)+" files.");
			    addTreeToDB(elib_dir, 0);
                            setMsgArea(main.getDefaultMsgText());
                }
	    }
	};
	Thread thread = new Thread(setGUIFile);
	thread.setPriority(Thread.NORM_PRIORITY - 1);
	thread.start();
        enableMenus(true);
        setMsgArea(main.getDefaultMsgText());
    }
    public JTree jTreegetTree() {
	return(jTree1);
    }
    public int jTreeGetRowCount() {
        return(jTree1.getRowCount());
    }
    void jTable1SingleClick(String filePath) {
        File selFile = new File(filePath);
	//Select TreePath in JTree for TableSelected
        String treePathStr = getTreePathFromFileName(filePath);
        int treeRows = jTree1.getRowCount();
        for (int i = 0; i < treeRows; i++) {
            if (jTree1.getPathForRow(i).toString().contains(treePathStr)) {
                jTree1.expandRow(i);
            }
            if (jTree1.getPathForRow(i).toString().equals(treePathStr)) {
                jTree1.setSelectionRow(i);
            }
        }
        if(selFile.isDirectory()){
            try {
                Image defaultImage = ImageIO.read(main.getURL("eLibrary.jpg"));
                setImagePanel(defaultImage);
            } catch(IOException ex) {}	
	}
        //PDF view the first page in imagePanel
//        if(filePath.endsWith(".pdf")){
//	    try {
//		PDDocument document = PDDocument.load(filePath);
//Get Document Language -> document.getDocumentCatalog().getLanguage();
//Set Document Language -> document.getDocumentCatalog().setLanguage("English");
//Boolean Document is Encrypted -> document.getDocument().isEncrypted();
//                int numOfPages = (int) document.getDocumentCatalog().getPages().getCount();
//System.out.println("Header = "+document.getDocument().getHeaderString());
//System.out.println("PageFormat 1 = "+document.getPageFormat(1));
//		
//                document.close();
////		pdfGetNumOfPages(filePath);
//		main.getDB().DBstatement("UPDATE ELIBRARY.BOOKS SET PAGES = "+numOfPages+" WHERE FILE = '"+filePath+"'");
//		
//		//Update Fields
//		pagesTextField.setText(String.valueOf(numOfPages));
//	    } catch (IOException ex) {
//		Logger.getLogger(GUI_Main.class.getName()).log(Level.SEVERE, null, ex);
//	    }
//	}
    }
    void jTable1DoubleClick(String filePath, boolean override) {
	try {
            File testfile = new File(filePath);
            if(testfile.isFile() || override) {
                String[] cmd = new String[1];
                cmd[0] = filePath;
		main.setCursor("Wait");
                Runtime.getRuntime().exec("cmd /c start \"\" \"" + filePath + "\"");
                try {
                    if (jTable1.getSelectedRowCount() == 1) Thread.sleep(3000);
		    main.setCursor("Default");
                } catch (InterruptedException ex) {
                    Logger.getLogger(GUI_Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            } 
	} catch (IOException ex) {
	    Logger.getLogger(GUI_Main.class.getName()).log(Level.SEVERE, null, ex);	    
	}
    }
    void jTree1SingleClick(TreePath selPath) {
	createjTable(getFileFromTreePath(selPath));
    }
    void jTree1DoubleClick(TreePath selPath, boolean override) {
	try {
	    setMsgArea("Loading File...");
            String path = getFileNameFromTreePath(selPath);
	    String[] cmd = new String[1];
            File testfile = new File(path);
	    if(testfile.isFile() || override) {
                cmd[0] = path;
                Runtime.getRuntime().exec("cmd /c start \"\" \"" + path + "\"");
                try {
                    Thread.sleep(3000);
		} catch (InterruptedException ex) {
                    Logger.getLogger(GUI_Main.class.getName()).log(Level.SEVERE, null, ex);
		}
	    }
        } catch (IOException ex) {
	    Logger.getLogger(GUI_Main.class.getName()).log(Level.SEVERE, null, ex);
	}
	setMsgArea(main.getDefaultMsgText());
	main.setCursor("Default");
    }
    void pdfGetNumOfPages(final String file){
    String dbpages = main.getDB().DBselect("select PAGES from ELIBRARY.BOOKS where FILE = '"+file+"'").get(0).toString();
	if(dbpages == null && !file.equals("null")) {
            int numOfPages = 0;
            pagesTextField.setText("");
            try {
                PDDocument document = PDDocument.load(file);
                    numOfPages = document.getNumberOfPages();
                document.close();
                main.getDB().DBstatement("UPDATE ELIBRARY.BOOKS SET PAGES = "+numOfPages+" WHERE FILE = '"+file+"'");
                if(jTable1.getSelectedRowCount() > 0){
                    if(jTable1.getModel().getValueAt(jTable1.getSelectedRow(), 4).equals(file)) pagesTextField.setText(String.valueOf(numOfPages));
                }
            } catch (IOException ex) {
                Logger.getLogger(GUI_Main.class.getName()).log(Level.SEVERE, null, ex);
            }
	} else if (jTable1.getSelectedRowCount() > 0) {
	    if(jTable1.getModel().getValueAt(jTable1.getSelectedRow(), 4).equals(file)) {
		pagesTextField.setText(dbpages);
	    }
	}
    }
    public void refreshTree(){
        File dir = new File(configs.get("ROOTDIR").toString());
        File[] files = dir.listFiles();
        int treerows = jTree1.getRowCount();
	TreePath[] paths = new TreePath[treerows];
	TreePath selectedpath = null;
	boolean selected = false;
	for (int i=0;i < treerows;i++) {
	    if(getExpansionState(jTree1, i)) {
                paths[i] = jTree1.getPathForRow(i);
		if (paths[i].equals(jTree1.getSelectionPath())) {
		    selectedpath = jTree1.getSelectionPath();
		    selected = true;
		}
            }
	}
        createjTreeModel(files);
        for (int i=0;i < jTree1.getRowCount();i++) {
	    for (int b=0;b < paths.length;b++) {
                if(paths[b] != null) {
		    if(paths[b].toString().equals(jTree1.getPathForRow(i).toString())) {
                        jTree1.expandRow(i);
		    }
		}
	    }
	    if(selected) {
                if(jTree1.getPathForRow(i).toString().equals(selectedpath.toString())) {
                    jTree1.setSelectionRow(i);
                }
	    }
	}
        setMsgArea(main.getDefaultMsgText());
    }
    public void restoreTree(TreePath[] paths) {
        for (int i=0;i < jTree1.getRowCount();i++) {
            for (int b=0;b < paths.length;b++) {
                if(paths[b] != null) {
                    if(paths[b].toString().equals(jTree1.getPathForRow(i).toString())) {
                                jTree1.expandRow(i);
                    }
                }
            }
        }	
    }
    void setImagePanel(Image image) {
	Graphics g = imagePanel.getGraphics();
        int hint = Image.SCALE_AREA_AVERAGING;
        Image thumbnail = image.getScaledInstance(imagePanel.getWidth(),imagePanel.getHeight(), hint);
        g.drawImage(thumbnail,0,0,null);
        imagePanel.paintComponents(g);
    }
    void setjTreeSelect(int row){
	jTree1.setSelectionRow(row);
    }
    void setMsgArea(String Msgtext) {
	if(Msgtext != null) {
            MsgArea.setText(Msgtext);
	}
    }
    void setMsgAreaBG(Color bg) {
	if(bg != null) {
	    MsgArea.setBackground(bg);
	}
    }      
    void setProgressBarUnknownLength(boolean x) {
	jProgressBar1.setIndeterminate(x);
    }
    void setProgressBarTask(int task) {
	jProgressBar1.setValue(task);
    }
    void setProgressBarTasks(int tasks) {
	jProgressBar1.setMaximum(tasks);
    }
    void setSplitPaneDeviderInteger(int width) {
	jSplitPane1.setDividerLocation(width);
    }
    void setSplitPane2DeviderInteger(int height) {
	jSplitPane2.setDividerLocation(height);
    }
    void setTabPane1jPanelForeGroundColor(Color fg) {
	readCheckBox.setForeground(fg);
	ratingSlider.setForeground(fg);
	titleLable.setForeground(fg);
	pagesLabel.setForeground(fg);
	authorLabel.setForeground(fg);
	editionLabel.setForeground(fg);
	languageLabel.setForeground(fg);
	publisherLabel.setForeground(fg);
	pubdateLabel.setForeground(fg);
	
    }
    void setTabPane1jPanelBackGroundColor(Color bg) {
	jTabbedPane1.setBackground(bg);
    
    Tab1Panel.setBackground(bg);
	readCheckBox.setBackground(bg);
	ratingSlider.setBackground(bg);
	imagePanel.setBackground(bg);
	
    }
    void setjTree1BackGroundColor() {
	refreshTree();
    }
    void setMenuPanelColors() {
	Color[] mcolors = main.getColors();
	jMenuBar1.setBackground(mcolors[13]);
	jMenuBar1.setForeground(mcolors[14]);
	MenuFile.setBackground(mcolors[13]);
	MenuFile.setForeground(mcolors[14]);
	MenuOptions.setBackground(mcolors[13]);
	MenuOptions.setForeground(mcolors[14]);
	MenuDatabase.setBackground(mcolors[13]);
	MenuDatabase.setForeground(mcolors[14]);
	MenuHelp.setBackground(mcolors[13]);
	MenuHelp.setForeground(mcolors[14]);
    }
    void setPopupMenuColors() {
        createimagePanelPopupMenu();
	createjTreePopupMenu();
	createjTablePopupMenu();
    }
    void setSplitPaneColor() {
	Color[] bgcolor = main.getColors();
        setSplitPaneDividerColor(jSplitPane1, bgcolor[15]);
        setSplitPaneDividerColor(jSplitPane2, bgcolor[15]);
    }
    void setSplitPaneDividerColor(JSplitPane splitPane, Color newDividerColor) {
        SplitPaneUI splitUI = splitPane.getUI();
        if (splitUI instanceof BasicSplitPaneUI) { // obviously this will not work if the ui doen't extend Basic...
            BasicSplitPaneDivider div = ((BasicSplitPaneUI) splitUI).getDivider();
            assert div != null;
            Border divBorder = div.getBorder();
            Border newBorder = null;
            Border colorBorder = null;
            class BGBorder implements Border {
                private Color color;
                private final Insets NO_INSETS = new Insets(0, 0, 0, 0);

                private BGBorder(Color color) {
                   this.color = color;
                }
                Rectangle r = new Rectangle();
                    @Override
                public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
                   g.setColor(color);
                   g.fillRect(x, y, width, height);
                   if (c instanceof Container) {
                      Container cont = (Container) c;
                      for (int i = 0, n = cont.getComponentCount(); i < n; i++) {
                         Component comp = cont.getComponent(i);
                         comp.getBounds(r);
                         Graphics tmpg = g.create(r.x, r.y, r.width, r.height);
                         comp.paint(tmpg);
                         tmpg.dispose();
                      }
                   }
                }

                    @Override
                public Insets getBorderInsets(Component c) {
                   return NO_INSETS;
                }

                    @Override
                public boolean isBorderOpaque() {
                   return true;
                }
            }
            colorBorder = new BGBorder(newDividerColor);

            if (divBorder == null) {
                newBorder = colorBorder;
            } else {
                newBorder = BorderFactory.createCompoundBorder(divBorder, colorBorder);
            }
            div.setBorder(newBorder);
        }
    }
    void setWinTitle(String title) {
    setTitle(title);
    }
    
    
class JTreeToolTipRenderer extends DefaultTreeCellRenderer {
	@Override
    public Component getTreeCellRendererComponent(  JTree tree, 
                                                    Object value, 
						    boolean sel, 
						    boolean expanded, 
						    boolean leaf, 
						    int row, 
						    boolean hasFocus) {
	super.getTreeCellRendererComponent( tree, value, sel, expanded, leaf, row, hasFocus );
        String tooltext = null;
        try {
            File mousefile = getFileFromTreePath(jTree1.getClosestPathForLocation(jTree1.getMousePosition().x, jTree1.getMousePosition().y));
            String rootPath = configs.get("ROOTPATH").toString();
            String prePath = jTree1.getSelectionPath().toString().replaceAll("]", "");
            String[] subpath = prePath.split(", ");
            String path = rootPath+System.getProperty("file.separator");
            for (int i = 0; i < jTree1.getSelectionPath().getPathCount(); i++) {
                if (i != 0) {
                    if (i < (jTree1.getSelectionPath().getPathCount() - 1)) {
                        path = path + subpath[i] + System.getProperty("file.separator");
                    } else {
                        path = path + subpath[i];
                    }
                }
            }
	    if(!path.equals(mousefile.getAbsolutePath())) {
                if(mousefile.isDirectory()) {
                    File[] files = mousefile.listFiles();
                    tooltext = "<HTML><BODY>";
                    for (int i=0;i < files.length;i++) {
                        tooltext = tooltext+"<img src=\""+main.getFileURL(files[i])+"\"/>&nbsp;<B>"+files[i].getName()+"</B><BR>";
                    }
                    tooltext = tooltext+"</BODY></HTML>";
                }
	    }
	} catch(Exception ex) {
	    //
	}
	setToolTipText(tooltext);
	
	return this;
    }
    }
class CustomDataRenderer extends DefaultTableCellRenderer {
	@Override
 public Component getTableCellRendererComponent( 
                    JTable jt ,
		    Object value, 
		    boolean isSelected,
                    boolean hasFocus,
		    int row, 
		    int column) {
     Component comp = super.getTableCellRendererComponent(jt ,value,isSelected,hasFocus,row,column);
        JLabel label = (JLabel)comp;
        colors = main.getColors();
	if(row % 2 == 0) {
	    setForeground(colors[0]);
	    setBackground(colors[2]);
	} else {
	    setForeground(colors[0]);
	    setBackground(colors[3]);
	}
	if (isSelected && row % 2 == 0) {
	    setForeground(colors[1]);
	    setBackground(colors[4]);
	} else if (isSelected && row % 2 != 0) {
            setForeground(colors[1]);
	    setBackground(colors[5]);
	}
	if(column == 1) {
	    label.setHorizontalAlignment(JLabel.LEFT);
	    File file = new File(jTable1.getModel().getValueAt(row, 4).toString());
	    String tooltext = null;
	    if(file.getName().endsWith(".zip") || file.getName().endsWith(".gzip") || file.getName().endsWith(".gz")) {
		    try {
			tooltext = "<HTML><BODY>";
			ZipFile zipfile = new ZipFile(jTable1.getModel().getValueAt(row, 4).toString());
			int zipfilesize = zipfile.size();
			if(zipfilesize == 1) {
                            tooltext += "<B>"+zipfilesize+" file</B><BR>";			    
			} else {
			    tooltext += "<B>"+zipfilesize+" files</B><BR>";
			}
			for (int i=0;i<zipfilesize;i++){
			    tooltext += "<B>"+zipfile.entries().nextElement().getName()+"</B><BR>";
                            if (i>5) break;
			}
			tooltext += "</BODY></HTML>";
		    } catch (IOException ex) {
			Logger.getLogger(GUI_Main.class.getName()).log(Level.SEVERE, null, ex);
		    }
	    }
	    setToolTipText(tooltext);
	} else if(column == 2){
	    label.setHorizontalAlignment(JLabel.RIGHT);
	} else if(column == 3){
	    label.setHorizontalAlignment(JLabel.CENTER);
	}
 	return label;                   
    }
    }
class CustomTableDataRenderer extends DefaultTableCellRenderer {
    Rectangle paintingRect = null;
    GradientPaint gp = null;    
    public CustomTableDataRenderer() {
        super();
        setOpaque(false);
        BevelBorder border = (BevelBorder) BorderFactory.createBevelBorder(
                BevelBorder.RAISED, 
                colorMap.get("menu"), 
                colorMap.get("menu").brighter(), 
                colorMap.get("header"), 
                colorMap.get("header").darker());                     
        setBorder((border));
    }   
    @Override
    public Component getTableCellRendererComponent(
         JTable jt,
         Object value,
         boolean isSelected,
         boolean hasFocus,
         int row,
         int column
         ){
        Component comp = super.getTableCellRendererComponent(jt,value,isSelected,hasFocus,row,column);
        Color tabColor = Color.GRAY;
        ImageIcon icon = new ImageIcon();
        setIcon(icon);
        setIconTextGap(6);        
        Rectangle rect = jt.getCellRect(row, column, isSelected);
        paintingRect = rect;
        setFont(fontMap.get("GA11B"));
        setForeground(colorMap.get("black"));
        int[] sels = jt.getSelectedRows();
        TableModel model = jt.getModel();
        if(row % 2 != 0) {
                tabColor = tabColor.brighter();
        }
        if(isSelected) {
            if(value instanceof String) setFont(fontMap.get("GA14B"));
            if(rect != null) {
                    if(sels[0] == row || sels.length == 1) {            
                        gp = new GradientPaint(0,0,tabColor.darker(),0,rect.height/2,tabColor.brighter());
                    } else if(sels[jt.getSelectedRowCount() - 1] == row) {
                        gp = new GradientPaint(0,0,tabColor.brighter(),0,rect.height/2,tabColor.darker());
                    } else {
                        gp = new GradientPaint(0,0,tabColor.brighter(),0,rect.height/2,tabColor.brighter());
                    }
                }
        } else if(!isSelected) {
            if(rect != null) {
                gp = new GradientPaint(0,0,tabColor.brighter(),(rect.width * 7)/8,0,tabColor.darker());
            }
        }
        int[] colAlignments = new int[]{4,2,2,2,2};
        setHorizontalAlignment(colAlignments[column]);
	if(value instanceof File){
              File file = (File)value;
              setIcon(getSystemFileIcon(file));
        }
        // ToolTip  (Middle MouseWheel Button Activates)
        Color bg = colorMap.get("menu");
        if(value != null){
            String tooltext =   "<HTML><BODY>"+
                                "<TABLE BORDER CELLPADDING=8 BGCOLOR=\"#"+Integer.toHexString(bg.getRGB()&0x00ffffff)+"\">"+
                                "<TH><H3>"+
                                jt.getModel().getColumnName(column)+
                                "</H3></TH><TD WIDTH=500><P STYLE=\"word-wrap:break-word;width:100%;left:0\"><B>"+
                                value.toString()+
                                "</B></P></TD></TABLE></BODY></HTML>";            
            setToolTipText(tooltext);
        }
        ToolTipManager.sharedInstance().setInitialDelay(3750);
        ToolTipManager.sharedInstance().setDismissDelay(30000);
        ToolTipManager.sharedInstance().setReshowDelay(1200);   
        return(comp);
    }
    public GradientPaint getColumnGradient() {
        return gp;
    }
    @Override
    public void paintComponent(Graphics g) {
        Rectangle rect = paintingRect;
        Graphics2D g2 = (Graphics2D)g;
        g2.setPaint(gp);
        g2.fillRect( 0, 0, rect.width, rect.height ); 
        super.paintComponent(g);
    }
 } 
class CustomHeaderRenderer extends DefaultTableCellRenderer {
    Rectangle paintingRect = null,lpr = null; 
    JTableHeader header = null;
    GradientPaint gp = null, hoverGradient, columnGradient;
 
    public CustomHeaderRenderer() {
        super();
        setOpaque(false);
        setFont(fontMap.get("GA16B"));
        BevelBorder border = (BevelBorder) BorderFactory.createBevelBorder(
                BevelBorder.RAISED, 
                colorMap.get("menu"), 
                colorMap.get("menu").brighter(), 
                colorMap.get("header"), 
                colorMap.get("header").darker());                     
        setBorder((border));                      
    }   
    public void setColumnGradient(GradientPaint gp) {
        this.columnGradient = gp;
    }
    public GradientPaint getColumnGradient() {
        return columnGradient;
    }
    @Override
    public void paintComponent(Graphics g) {
        Rectangle rect = paintingRect;
 
        Graphics2D g2 = (Graphics2D)g;
            g2.setPaint(gp);
            g2.fillRect( 0, 0, rect.width, rect.height );
 
        super.paintComponent(g);
    }
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
        boolean hasFocus, int row, int col) {
        int[] colAlignments = new int[]{4,2,2,2,2};
        if(table.getName().equals("ViewTable")) colAlignments = new int[]{2,2,4,0,0};
        if(colAlignments != null) setHorizontalAlignment(colAlignments[col]);
        Rectangle rect = table.getTableHeader().getHeaderRect(col);
        gp = new GradientPaint(0, 0, colorMap.get("header").brighter(), 0, rect.height/2, colorMap.get("header"));
        setForeground(colorMap.get("black")); 
        paintingRect = rect;
        setText( value == null ? "" : value.toString() );
        ImageIcon icon = new ImageIcon();
        setIcon(icon);
        setIconTextGap(6);
        return(this);
    }
    }
class CustomTreeCellRenderer extends DefaultTreeCellRenderer {
        
        public CustomTreeCellRenderer() 
        { 
            super(); 
        } 
        @Override
	public Component getTreeCellRendererComponent(JTree tree,
                                                      Object value,
                                                      boolean selection, 
						      boolean expanded,
                                                      boolean leaf, 
						      int row, 
						      boolean hasFocus) { 
            super.getTreeCellRendererComponent(tree, value, selection, expanded, leaf, row, hasFocus); 
            DefaultMutableTreeNode node = (DefaultMutableTreeNode)value; 
	    setIconAndToolTip(node.getUserObject(), tree, expanded, leaf, selection);
            return(this);
	}
	private void setIconAndToolTip(Object obj, JTree tree, boolean expanded, boolean leaf, boolean selected) {
		setFont(font);
		Color[] colors = main.getColors();
                tree.setBackground(colors[10]);
                setForeground(colors[11]);
		setBackgroundNonSelectionColor(colors[10]);
		setBackgroundSelectionColor(colors[12]);
                Icon icon = null;
		if(obj instanceof String) {
		String str = (String)(obj);
		if(str.equals("eLibrary")) {
		    if(expanded) {
                        icon = new ImageIcon(main.getURL("book_opened.gif"));
		    } else {
                        icon = new ImageIcon(main.getURL("book.gif"));
                    }
		    setIcon(icon);
		} else {
                    if (leaf && selected){
                        icon = new ImageIcon(main.getURL("folder_open.gif"));			
                    } else if(expanded){
                        icon = new ImageIcon(main.getURL("folder_open.gif"));
                    } else {
                        icon = new ImageIcon(main.getURL("folder_close.gif"));
                    }
		    setIcon(icon);
		}
		BufferedImage bufferedIcon = null;
		try {
		    bufferedIcon = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_BYTE_INDEXED);
		} catch (Exception ex) {
		}
		Graphics g = bufferedIcon.getGraphics();
		iconPaint(g);
	    }
	}
        @Override
	public Color getBackgroundNonSelectionColor() {
	    return super.getBackgroundNonSelectionColor();
	}
	@Override
	public Color getBackgroundSelectionColor() {
	    return super.getBackgroundSelectionColor();
	}
	@Override
	public synchronized DropTarget getDropTarget() {
	    return super.getDropTarget();
	}
	public int getNEXT() {
	    return NEXT;
	}
	public int getPREVIOUS() {
	    return PREVIOUS;
	}
	@Override
	public void setBackgroundNonSelectionColor(Color newColor) {
	    super.setBackgroundNonSelectionColor(newColor);
	}
	@Override
	public void setBackgroundSelectionColor(Color newColor) {
	    super.setBackgroundSelectionColor(newColor);
	}

	@Override
	public void setForeground(Color fg) {
	    super.setForeground(fg);
	}

	public void iconPaint( Graphics g ) {
            Color[] colors = main.getColors();
	    Color bColor = colors[10];
            g.fillRect( 0, 0, getWidth() - 1, getHeight() - 1 );
            g.setColor(bColor);
//            super.paint( g );
	    this.paint(g);
	}
    }   
 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem ColorMenu;
    private javax.swing.JCheckBoxMenuItem Debug;
    private javax.swing.JMenuItem Exit;
    private javax.swing.JMenu MenuDatabase;
    private javax.swing.JMenu MenuFile;
    private javax.swing.JMenu MenuHelp;
    private javax.swing.JMenu MenuOptions;
    private javax.swing.JTextField MsgArea;
    private javax.swing.JMenuItem SetCatRoot;
    private javax.swing.JPanel Tab1Panel;
    private javax.swing.JComboBox authorComboBox;
    private javax.swing.JLabel authorLabel;
    private javax.swing.JMenuItem dbClean;
    private javax.swing.JMenuItem dbConnect;
    private javax.swing.JMenuItem dbDisconnect;
    private javax.swing.JLabel editionLabel;
    private javax.swing.JSpinner editionSpinner;
    private javax.swing.JPanel imagePanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenuTable;
    private javax.swing.JPopupMenu jPopupMenuTree;
    private javax.swing.JPopupMenu jPopupMenuimagePanel;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTree jTree1;
    private javax.swing.JComboBox languageComboBox;
    private javax.swing.JLabel languageLabel;
    private javax.swing.JScrollPane notesScrollPane;
    private javax.swing.JTextArea notesTextArea;
    private javax.swing.JLabel pagesLabel;
    private javax.swing.JTextField pagesTextField;
    private javax.swing.JLabel pubdateLabel;
    private javax.swing.JTextField pubdateTextField;
    private javax.swing.JComboBox publisherComboBox;
    private javax.swing.JLabel publisherLabel;
    private javax.swing.JSlider ratingSlider;
    private javax.swing.JCheckBox readCheckBox;
    private javax.swing.JLabel titleLable;
    private javax.swing.JTextField titleTextField;
    // End of variables declaration//GEN-END:variables

}
