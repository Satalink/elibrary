
package elibrary;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Map;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.filechooser.FileSystemView;

public class eMain implements Interface {
    private String PROGNAME = "eLibrary";
    private String pathdiv = System.getProperty("file.separator");
    private String curDir = System.getProperty("user.dir");
    private double VERNUM = 0.3;
    private String VERSION = "v"+VERNUM+"";
    public GUI_Main eGUI = null;
    private JFileChooser fc;
    public InputBox eMSG = null;
    protected String MsgText;
    protected URL refURL = null;
    protected String refFile = null;
    protected DB eDB;
    protected WebMatrix eWeb;
    protected Color[] colors = {   //     Initialize
        new Color(0,0,0),               //0 -> Normal Font Color
        new Color(0,0,0),               //1 -> Selected Font Color
        new Color(75,155,255),          //2 -> Primary Table Color
        new Color(205,205,205),         //3 -> Alternate Table Color
        new Color(45,125,195),          //4 -> Primary Selected Table Color
        new Color(175,175,175),         //5 -> Alternate Table Color
	new Color(170,170,170),         //6 -> Table Header Color
	new Color(0,0,0),               //7 -> Table Header Font Color
	new Color(0,0,0),               //8 -> Tab1 JPanel Foreground Color
	new Color(70,135,235),          //9 -> Tab1 JPanel Background Color
	new Color(255,255,255),         //10-> Tree Background Color
	new Color(0,0,0),               //11-> Tree Foreground Color
	new Color(75,155,195),          //12-> Tree Selected Color
        new Color(205,205,205),         //13-> Menu Background Color
	new Color(0,0,0),               //14-> Menu Foreground Color
	new Color(205,205,205),         //15-> SplitPane Background Color
	new Color(235,235,235),         //16-> SplitPane Foreground Color
	new Color(175,175,175),         //17-> Popup Menus Background Color
	new Color(0,0,0)                //18-> Popup Menus Foreground Color
        };
    
    public eMain() throws IOException {
	eDB = new DB(this);
//	eWeb = new WebMatrix(this);
        fc = new JFileChooser();
        eGUI = new GUI_Main(this);
	GUI_Main.setDefaultLookAndFeelDecorated(true);
	eGUI.setVisible(false);
	refFile = "bookStack.gif";
	Image winicon = new ImageIcon(getURL(refFile)).getImage();
	eGUI.setIconImage(winicon);
	if(eDB.DBexists()) {
	    if(eDB.DBconnect()) {
		eGUI.setMsgArea(MsgText);     
        eGUI.getConfigs();
        Map configsMAP = eGUI.configs;
        int coordx = Integer.valueOf(configsMAP.get("GUIPOS_X").toString());
        int coordy = Integer.valueOf(configsMAP.get("GUIPOS_Y").toString());
        int spcoord = Integer.valueOf(configsMAP.get("SPLITPANE_X").toString());
        int spcoord2 = Integer.valueOf(configsMAP.get("SPLITPANE_Y").toString());
        int winsizeh = Integer.valueOf(configsMAP.get("GUIDIM_H").toString());
        int winsizew = Integer.valueOf(configsMAP.get("GUIDIM_W").toString());
        String[] treestates = configsMAP.get("TREESTATES").toString().split(",");
        String[] colorValues = configsMAP.get("COLORS").toString().split("\\|\\|");
		colors = new Color[colorValues.length];
		int[] colorVals = new int[3];
		for (int i=0;i<colorValues.length;i++){
		    String[] intStr = colorValues[i].split(",");
		    for (int digit=0;digit<intStr.length;digit++){
			colorVals[digit] = Integer.valueOf(intStr[digit]);
		    }
                    colors[i] = new Color(colorVals[0],colorVals[1],colorVals[2]);
		}
		eGUI.setLocation(coordx, coordy);
		eGUI.setSize(winsizew, winsizeh);
		eGUI.setSplitPaneDeviderInteger(spcoord);
		eGUI.setSplitPane2DeviderInteger(spcoord2);
                File dir = new File(configsMAP.get("ROOTDIR").toString());
		File[] files = dir.listFiles();		
		eGUI.createjTreeModel(files);
		for (int i=0;i < treestates.length;i++) {
		    if(treestates[i].equals("1")) eGUI.expandJTree(i);
		}
		eGUI.createjTable(dir);
		eGUI.setTabPane1jPanelForeGroundColor(colors[8]);
		eGUI.setTabPane1jPanelBackGroundColor(colors[9]);
		eGUI.setMenuPanelColors();
		eGUI.setPopupMenuColors();
		eGUI.setVisible(true);
		eGUI.setTitle(getDefaultTitle());
	    } else {
		try {
		    eGUI.setVisible(true);
		    eGUI.setMsgAreaBG(Color.RED);
		    eGUI.setMsgArea("DB Failed! Close any other existing connections.");
		    Thread.sleep(3000);
		    System.exit(1);
		} catch (InterruptedException ex) {
		    Logger.getLogger(eMain.class.getName()).log(Level.SEVERE, null, ex);
		}
	    }
	} else {
	    File dir = new File(curDir);
            FileFilter dirFilter = new FileFilter() {
		@Override
                public boolean accept(File file) {
                    return file.isDirectory();
                }
            };
	    File[] files = dir.listFiles(dirFilter);
            eGUI.setLocation(80, 15);
            eGUI.setSize(1000, 800);
            eGUI.setSplitPaneDeviderInteger(180);
            eGUI.setSplitPane2DeviderInteger(380);
	    eGUI.createjTreeModel(files);
	    eGUI.setjTreeSelect(0);
	    eGUI.setVisible(true);
            eGUI.enableMenus(false);
	    Color msgareaColor = eGUI.getBackground();
	    Color bg = Color.green.darker();
	    eGUI.setMsgAreaBG(bg);
	    eGUI.setMsgArea("Creating DataBase... Please wait.");
	    eGUI.setProgressBarUnknownLength(true);
	    if(eDB.DBconnect()) {
                eGUI.setMsgAreaBG(msgareaColor);
		eGUI.setMsgArea("DB Connected");
                eGUI.enableMenus(true);
		eGUI.jFileChooserSelect();
	    } else {
		try {
		    bg = Color.red;
		    eGUI.setMsgAreaBG(bg);
		    eGUI.setMsgArea("DB Failed");
		    Thread.sleep(3000);
//		    System.exit(1);
		} catch (InterruptedException ex) {
		    Logger.getLogger(eMain.class.getName()).log(Level.SEVERE, null, ex);
		}
		
	    }
        }
        setMsgArea(getDefaultMsgText());
	//Graphics need to be last loaded!
        try {
            Image defaultImage = ImageIO.read(getURL("eLibrary.jpg"));
            eGUI.setImagePanel(defaultImage);
            } catch(IOException ex) {}
	
    }

    public DB getDB() {
		return(eDB);
    }
    public WebMatrix getWeb() {
		return(eWeb);
	}
    public static void main(String[] args) throws Exception {
	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	eMain e = new eMain();
//	e.License();
    } 
    public void exitApp() {
	eGUI.setVisible(false);
	// Get Tree Expansions
        JTree tree = eGUI.jTreegetTree();
        int treerows = eGUI.jTreeGetRowCount();
        String states = "";
	int state = 0;
	for (int i=0;i < treerows;i++) {
      	    if(eGUI.getExpansionState(tree, i)) { 
		state=1;
	    } else {
		state=0;
	    }
	    states += state+",";
	} 
	if (getDB().DBconnect()) {
	    String sql;
	    String colorString = "";
	    for (int i=0;i<colors.length;i++){
		colorString += String.valueOf(colors[i].getRed())+","+
                               String.valueOf(colors[i].getGreen())+","+
			       String.valueOf(colors[i].getBlue());
		if(i != (colors.length-1)) colorString += "||";
	    }
	    sql = "UPDATE ELIBRARY.CONFIGS "+
		  "SET "+
		  "GUIPOS_X = "+getWinCoord().x+", "+
		  "GUIPOS_Y = "+getWinCoord().y+", "+
		  "SPLITPANE_X = "+getSplitPaneDeviderCoord()+", "+
		  "SPLITPANE_Y = "+getSplitPane2DeviderCoord()+", "+
		  "GUIDIM_H = "+getWinSize().height+", "+
		  "GUIDIM_W = "+getWinSize().width+", "+
		  "TREESTATES = '"+states+"', "+
		  "TABLE_COLS = '"+eGUI.getColumnWidth(0)+"."+eGUI.getColumnWidth(1)+"."+eGUI.getColumnWidth(2)+"."+eGUI.getColumnWidth(3)+".0', "+
          "COLORS = '"+colorString+"' "+
		  "WHERE ID = 1";
        getDB().DBstatement(sql);
        getDB().DBshutdown();
	}
	System.exit(0);	
    }
    protected String getHostname() {
	String hostname = null;
		try {
			InetAddress addr = InetAddress.getLocalHost();
			hostname = addr.getHostName();
		} catch (UnknownHostException ex) {
			ex.printStackTrace();
		}
	return(hostname);
    }
    protected String getJversion() {
	String jVersion = System.getProperty("java.version");
	return(jVersion);
    }
    protected String getOSName() {
	String osName = System.getProperty("os.name");
	return(osName);
    }
    protected String getUserHome() {
	String usrHome = System.getProperty("user.home");
	return(usrHome);
    }   
    public boolean getClipboardAccess() {
	boolean sma = false;
        SecurityManager sm = System.getSecurityManager();
        try {
          if (sm != null) {
            sm.checkSystemClipboardAccess();
          }
          sma = true;
        } catch (SecurityException ex) {
          sma = false;
        }
	return(sma);
    }
    public String getDefaultMsgText() {
	int filecount = 0;
	String value = PROGNAME+" "+VERSION;
	if(eDB.DBexists() && eDB.DBconnect()) {
        int dbcount = eDB.SQL_selectCount("ELIBRARY.BOOKS");
        File file = new File(eGUI.configs.get("ROOTDIR").toString());
        if(dbcount == 0) {
            filecount = 0;
	    } else {
            filecount = igetRecursiveFileCount(file);
	    }
        if(dbcount > filecount) {
            getDB().DBclean();
        } else if(filecount > dbcount) {
            final File rootdir = new File(eGUI.configs.get("ROOTDIR").toString());
            Runnable UpdateDB = new Runnable() {
            @Override
                public void run() {
                            eGUI.addTreeToDB(rootdir, 0);			
                }
            };
            Thread thread = new Thread(UpdateDB);
            thread.setPriority(Thread.NORM_PRIORITY -1);
            thread.start();
	    }
	    value = filecount+" eBooks";
	}
        return(value);
    }
    public String getDefaultTitle() {
	String title = "eLibrary "+VERSION+"  UNREGISTERED";
	if(License()) {
	    title = "eLibrary "+VERSION+"  REGISTERED";	 
	}
        return(title);
    }
    public JFileChooser getFileChooser() {
        return(fc);
    }  
    public float igetFileSize(File file) {
	float size = 0;
	if(file.isFile()) {
	    size = ((float)file.length()/(float)1000000);
	} else if(file.isDirectory()) {
	    float tsize = 0;
	    File[] flist = file.listFiles();
	    for (int i=0;i < flist.length;i++) {
		File tfile = new File(flist[i].getAbsolutePath());
		if(tfile.isFile()) tsize+=((float)tfile.length()/(float)1000000);
		else { tsize+=igetFileSize(tfile); }
	    }
            size = tsize;
	}
	return(size);
    }
    public URL getFileURL(File file){
	URL imgurl;
	if(file.isDirectory()) {
	    imgurl = getURL("folder_close.gif");
	} else if(file.getName().toLowerCase().endsWith(".pdf")) {
	    imgurl = getURL("pdf.gif");
	} else if(file.getName().toLowerCase().endsWith(".chm")) {
	    imgurl = getURL("chm.gif");
	} else if(file.getName().toLowerCase().endsWith(".doc")) {
	    imgurl = getURL("doc.gif");
	} else if(file.getName().toLowerCase().endsWith(".ppt")) {
	    imgurl = getURL("ppt.gif");
	} else if(file.getName().toLowerCase().endsWith(".rar")) {
	    imgurl = getURL("rar.gif");
	} else if(file.getName().toLowerCase().endsWith(".par2")) {
	    imgurl = getURL("rar.gif");
	} else if(file.getName().toLowerCase().endsWith(".par")) {
	    imgurl = getURL("rar.gif");
	} else if(file.getName().toLowerCase().endsWith(".zip")) {
	    imgurl = getURL("zip.gif");
	} else {
	    imgurl = getURL("file.gif");
	}
	return(imgurl);
    }    
    public Color[] getColors() {
	return(colors);
    }
    public String getUserName() {
	String osUser = System.getProperty("user.name");
	return(osUser);
    }
    public URL getURL(String link) {
	String UniRefLink = null;
	JarFile mainjar = null;
	refURL = null;
	try {
	    mainjar = new JarFile(PROGNAME+".jar");
	} catch (Exception e) {
	    e.printStackTrace();
	}
	Enumeration entries = mainjar.entries();
	while (entries.hasMoreElements()) {
	    String curURL = entries.nextElement().toString();
	    if(curURL.contains(link)) {
		UniRefLink = curURL;
		break;
	    }
	}
        try {
	    refURL = new URL("jar:file:/"+curDir+pathdiv+PROGNAME+".jar!/"+UniRefLink);
	    } catch (MalformedURLException ex) {
		    ex.printStackTrace();
	    }
	return(refURL);
    }
    public Icon getSystemFileIcon(File file) {
        FileSystemView view = FileSystemView.getFileSystemView();
	return(view.getSystemIcon(file));
    }
    public int getSplitPaneDeviderCoord() {
	return(eGUI.getSplitPaneDeviderInteger());
    }
    public int getSplitPane2DeviderCoord() {
	return(eGUI.getSplitPane2DeviderInteger());
    }
    public Point getWinCoord() {
	Point wincoord = eGUI.getLocation();
	return(wincoord);
    }
    public Dimension getWinSize() {
	return(eGUI.getSize());
    }
    public boolean icopyFile (File fromFile, File toFile) throws FileNotFoundException {
	boolean status = false;
	try {
	    FileInputStream fis = new FileInputStream(fromFile);
	    FileOutputStream fos = new FileOutputStream(toFile);
	    byte[] buf = new byte[1024];
	    int i = 0;
	    while ((i = fis.read(buf)) != -1) {
		fos.write(buf, 0, i);
	    }
	    fis.close();
	    fos.close();
	    if (toFile.exists()) {
		status = true;
	    }
	} catch (IOException ex) {
	    Logger.getLogger(eMain.class.getName()).log(Level.SEVERE, null, ex);
	}
        return status;
    }
    public boolean ideleteFile(File file) {
        boolean status = true;
        if (file.isDirectory()) {
            String[] children = file.list();
            for (int i=0; i<children.length; i++) {
                File dfile = new File(children[i]);
                if(dfile.exists() && dfile.isFile() && dfile.canWrite()) {
                    status = ideleteFile(dfile);
                } else if(dfile.exists() && dfile.isDirectory() && dfile.canWrite()) {
                    status = ideleteFile(dfile);
                }
                if(status) setMsgArea("Deleted "+file.getName());
                if(!status) {
                    setMsgArea("Failed Delete "+file.getName());
                    break;
                }
            }
        }
        // The directory is now empty so delete it
        if(status) {
            status = file.delete();
        }
        return(status);
    }
    public File igetFileFromPath(String path) {
        File file = new File(path);
	return(file);
    }
    public int igetRecursiveFileCount(File file) {
	int count=0;
	if (file.isDirectory()) {
	    String[] children = file.list();
            for (int i=0;i<children.length;i++) {
                count+=igetRecursiveFileCount(new File(file, children[i]));
	    }
	} else if (file.isFile()) count++;
	return(count);
    }
    public File irenameFile(File file){
	boolean status = false;
        if(file.toString().contains("'")||file.toString().contains(",")){
            String newName = null;
	    if(file.toString().contains("'") && file.toString().contains(",")) {
                newName = file.getAbsoluteFile().toString().replace("'", "");
		newName = newName.replace(",", ".");
	    }
	    else if(file.toString().contains("'")) {
		newName = file.getAbsoluteFile().toString().replace("'", "");
	    }
	    else if(file.toString().contains(",")) {
		newName = file.getAbsoluteFile().toString().replace(",", ".");
	    }
	    File newFile = new File(newName);
            status = file.renameTo(newFile);
            if(status) {
		file = newFile;
	    } else {
                //TODO Add errors to vector and display at the end in a MsgBox.
		System.out.println("File "+file.getAbsoluteFile().toString()+" could not be renamed");
	    }
	}
	return file;
    }
    public boolean License() {
	boolean validation = false;
	String licensePC = "NEAL";
	String licenseUsr = "Satalink";
        String licensePC2 = "LIFEBOOK";
        String licenseUsr2 = "Satalink";
	String User = getUserName();
	String hostname = getHostname();

    // Development OR Freeware
    boolean validPC = true;
    boolean validUsr = true;
    
//	boolean validPC = licensePC.contentEquals(hostname);
//	boolean validUsr = licenseUsr.contentEquals(User);
//	boolean validPC2 = licensePC2.contentEquals(hostname);
//	boolean validUsr2 = licenseUsr2.contentEquals(User);
	if(validPC && validUsr) {
	    validation = true;
	}
//    else if (validPC2 && validUsr2) {
//	    validation = true;
//        } else {
//	    validation = false;
//	    File dbFile = new File(getUserName()+"DB");
//	    if(dbFile.exists()) {
//            ideleteFile(dbFile);
//	    }
//	}
	return(validation);
    }
    public void setCursor(String c){
        if(c.equals("Default"))   eGUI.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	else if(c.equals("Wait")) eGUI.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
	else if(c.equals("Text")) eGUI.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
	else if(c.equals("Move")) eGUI.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
	else if(c.equals("Hand")) eGUI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }
    public void setMsgArea(String text) {
	eGUI.setMsgArea(text);
    }
    public void setProgressBarMax(int x) {
	eGUI.setProgressBarTasks(x);
    }
    public void setProgressBarCurrent(int x) {
	eGUI.setProgressBarTask(x);
    }
    public void setProgressBarIndeterminate(boolean x) {
	eGUI.setProgressBarUnknownLength(x);
    }
    public void setSplitPaneDeviderCoord(int w) {
	eGUI.setSplitPaneDeviderInteger(w);
    }
    public void setSplitPane2DeviderCoord(int h) {
	eGUI.setSplitPane2DeviderInteger(h);
    }
    public void setColors(Color[] inColors) {
	colors = inColors;
    }
    public void setWinTitle(String title){
	if(title.isEmpty()) {
	    title = getDefaultMsgText();
	}
	eGUI.setWinTitle(title);
    }
    public void enableMenu(boolean x) {
	eGUI.enableMenus(x);
    }
    public boolean deleteDir(File dir) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    public void getConfigs() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    public void setProgressBarPaintStr(boolean x) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}


