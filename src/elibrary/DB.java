/*
 * initDB.java
 *
 * Created on April 7, 2007, 6:09 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elibrary;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DB {
    // runtime variables.
    private Interface main;
    private String MsgText = "eLibrary";
    private String curDir = System.getProperty("user.dir");
    private String pathDiv = System.getProperty("file.separator");
    // database
    private String DBDriver = "org.apache.derby.jdbc.EmbeddedDriver"; 
    private String dbName;
    private String dbUsr;
    private String dbPass;
    private String dbProtocol = "jdbc:derby:";
    private String dbURL;
    private String dblogon;
    private String dbcreate;
    // jdbc Connection
    private Connection conn = null;
    private boolean status = false;
    
    public DB(Interface iface) {
	main = iface;
	dbName = "eLibrary"+"DB";
	dbUsr = "eLibrary";
	dbPass = "eLibrary";
	dbURL = dbProtocol+dbName;
	dblogon = dbURL+";user="+dbUsr+";password="+dbPass;
	dbcreate = dbURL+";create=true;user="+dbUsr+";password="+dbPass;
    }
    
    protected boolean DBexists() {
        return(new File(dbName)).exists();
    }
    protected boolean DBclose() {
        try {
            if (conn != null) {
                DriverManager.getConnection(dblogon+";shutdown=true");
                conn.close();
                conn = null;
                status = true;
            } else {
                status = true;
            }
        }
        catch (SQLException sqlExcept) {
            status = false;
        }
        return(status);
    }
    protected boolean DBconnect() {
	boolean exists = DBexists();
	if(exists && main.License()) {    
	    try {
		Class.forName(DBDriver).newInstance();
		conn = DriverManager.getConnection(dblogon);
		return(true);
	    } catch (Exception ex) {
		return(false);
	    }
	} else if(exists && !main.License()) {
		File dbFile = new File(dbName);
		main.ideleteFile(dbFile);
		return(false);
	} else {
	    try {
		Class.forName(DBDriver).newInstance();
		conn = DriverManager.getConnection(dbcreate);		
		boolean connstat = DBreconnect();
		if(connstat) {
		    main.setProgressBarIndeterminate(false);
		    DBinit();
		}
		return(connstat);
	    } catch (Exception ex) {
                System.out.println(dbcreate);
		return(false);
	    }
	}
    }  
    protected boolean DBreconnect() {
	boolean exists = DBexists();
	conn = null;
	if(exists) {
	    try {
		Class.forName(DBDriver).newInstance();
		conn = DriverManager.getConnection(dblogon);
		return(true);
	    } catch (Exception ex) {
		return(false);
	    }
	}
	return(false);
    }  
    public boolean DBstatement (String sql) {
        try {
            conn.createStatement().execute(sql);
            status = true;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, sql, ex);
            status = false;
        }
        return(status);
    }
    public ArrayList DBselect(String sqlstatement) {
        ArrayList<Map> resultList = new ArrayList<Map>();
        try {
            if(conn == null || conn.isClosed()) {
                DBconnect();
            }
            if(!conn.isClosed()) {
                Statement stmt = conn.createStatement();
                ResultSet resultset = stmt.executeQuery(sqlstatement);
                ResultSetMetaData rsmd = resultset.getMetaData();
                int columns = rsmd.getColumnCount();
                int i = 0;
                while (resultset.next()) {
                    int col = 1;
                    Map resultMap = new HashMap();
                    while (col <= columns) {
                        String value = resultset.getString(col);
                        String colName = rsmd.getColumnName(col);
                        resultMap.put(colName, value);
                        col++;
                    }
                    resultList.add(0, resultMap);
                    i++;
                }
                resultset.close();
                stmt.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resultList;
    }
    public int SQL_selectCount(String table) {
	int resultvalue = 0;
        String sql = "SELECT COUNT(*) from "+table;
	try {
		Statement stmt = conn.createStatement();
		ResultSet results = stmt.executeQuery(sql);
                if(results.next()) {
		    resultvalue = results.getInt(1);
		}
		results.close();
		stmt.close();
	} catch (SQLException ex) {
		resultvalue = 0;
	}
	return(resultvalue);
    }
    public int SQL_selectMaxforTable(String column, String table) {
        int resultvalue = 0;
	try {
            String sql = "SELECT MAX("+column+") FROM "+table;
            Statement stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(sql);
            if(results.next()) {
                resultvalue = results.getInt(1);
            }
            results.close();
            stmt.close();
	} catch (SQLException ex) {
	    resultvalue = 0;
	}
        return (resultvalue);
    }
    public boolean SQL_exists(String container,String query) {
	boolean exists = false;
	String sql = "SELECT "+container+" FROM "+query;
	try {
	    Statement stmt = conn.createStatement();
	    ResultSet results = stmt.executeQuery(sql);
	    if(results.next()) {
		exists = true;
	    }
            results.close();
	    stmt.close();
	} catch (SQLException ex) {
	    exists = false;
	}
	return(exists);
    }
    protected void DBclean() {
        Runnable setGUIFile = new Runnable() {
	    @Override
	    public void run() {
                main.setMsgArea("Cleaning Database...");
                ArrayList paths = DBselect("select FILE from ELIBRARY.BOOKS");
                main.setProgressBarMax(paths.size());
		for(int i=0;i < paths.size();i++) {
                    main.setProgressBarCurrent(i);
                    Map fileMap = (Map) paths.get(i);
                    File file = new File(fileMap.get("FILE").toString());
		    if(!file.exists()) {
                DBstatement("Delete from ELIBRARY.BOOKS where FILE = '"+file.getAbsolutePath()+"'");
		    } else {
		    //Remove Duplicates
                Map resultMap;
                resultMap = (Map) DBselect("Select Count(*) from ELIBRARY.BOOKS WHERE FILE = '"+file.getAbsolutePath()+"'").get(0);
                int dupcount = Integer.valueOf(resultMap.get("1").toString());
                if(dupcount > 1) {
                      resultMap = (Map) DBselect("Select Max(BOOK_ID) FROM ELIBRARY.BOOKS WHERE FILE = '"+file.getAbsolutePath()+"'").get(0);
                      int del_id = Integer.valueOf(resultMap.get("1").toString());
                      DBstatement("delete from ELIBRARY.BOOKS where BOOK_ID = "+del_id+"");
                }
            }
		}
		main.setProgressBarCurrent(paths.size());
                main.setMsgArea(main.getDefaultMsgText());
	    }
	};
        Thread thread = new Thread(setGUIFile);
        thread.setPriority(Thread.NORM_PRIORITY - 2);
        thread.start();	
    }
    protected void DBinit() {
	String sql;
	int tasks = 8;
	int task = 0;
	main.setMsgArea("Initializing Database");
	main.setProgressBarMax(tasks);
	// Creation of CONFIGS
	sql = 
	    "CREATE TABLE ELIBRARY.CONFIGS ("+
	    "ID INTEGER not null unique,"+
	    "GUIPOS_X INTEGER,"+
	    "GUIPOS_Y INTEGER,"+
	    "ROOTDIR VARCHAR(256),"+
	    "SPLITPANE_X INTEGER,"+
	    "GUIDIM_H INTEGER,"+
	    "GUIDIM_W INTEGER,"+
            "SPLITPANE_Y INTEGER,"+
	    "TREESTATES VARCHAR(8192),"+
            "TABLE_COLS VARCHAR(32),"+
            "COLORS VARCHAR(256))";
		DBstatement(sql);
	main.setProgressBarCurrent(task++);
	sql = "INSERT INTO ELIBRARY.CONFIGS VALUES ("+
	      "1"+","+
	      main.getWinCoord().x+","+
	      main.getWinCoord().y+","+
	      "'"+System.getProperty("user.dir")+"',"+
              main.getSplitPaneDeviderCoord()+","+
              main.getWinSize().height+","+
              main.getWinSize().width+","+
              main.getSplitPane2DeviderCoord()+","+
	      "'true,',"+
	      "'16.720.94.170',"+
	      "'0,0,0')";
		DBstatement(sql);
	// Creation of BOOKS
	main.setProgressBarCurrent(task++);
	sql = 
	   "CREATE TABLE ELIBRARY.BOOKS ("+
	    "BOOK_ID INTEGER not null primary key,"+
	    "COVER BLOB(256K),"+
	    "BOOKTITLE VARCHAR(128),"+
	    "LASTVIEWED DATE,"+
	    "FILE VARCHAR(256),"+
	    "URL VARCHAR(256),"+
	    "AUTHOR_ID INTEGER,"+
	    "PUBLISHER_ID INTEGER,"+
	    "PUBLICATION_DATE DATE,"+
	    "EDITION NUMERIC,"+
	    "GENRE_ID INTEGER,"+
	    "LANGUAGE_ID INTEGER,"+
	    "CATALOG_ID INTEGER,"+
	    "NOTES VARCHAR(1024),"+
	    "ISBN VARCHAR(16),"+
	    "READFLAG INTEGER,"+
	    "PAGES INTEGER," +
	    "RATING_ID INTEGER)";
	DBstatement(sql);
	main.setProgressBarCurrent(task++);
	sql =
	   "CREATE TABLE ELIBRARY.AUTHORS ("+
	    "AUTHOR_ID INTEGER,"+
	    "AUTHOR VARCHAR(256))";
	DBstatement(sql);
	main.setProgressBarCurrent(task++);
	sql =
	   "CREATE TABLE ELIBRARY.PUBLISHERS ("+
	    "PUBLISHER_ID INTEGER,"+
	    "PUBLISHER VARCHAR(256))";
	DBstatement(sql);
	main.setProgressBarCurrent(task++);
	sql = 
	    "CREATE TABLE ELIBRARY.GENRES ("+
	    "GENRE_ID INTEGER,"+
	    "GENRE VARCHAR(128))";
	DBstatement(sql);
	main.setProgressBarCurrent(task++);
	sql =
	    "CREATE TABLE ELIBRARY.LANGUAGES ("+
	    "LANUGAGE_ID INTEGER,"+
	    "LANGUAGE VARCHAR(128))";
	DBstatement(sql);
	main.setProgressBarCurrent(task++);
	sql =
	    "CREATE TABLE ELIBRARY.CATALOGS ("+
	    "CATALOG_ID INTEGER,"+
	    "CATALOG VARCHAR(256))";
	DBstatement(sql);
	main.setProgressBarCurrent(task++);
	sql = 
	    "CREATE TABLE ELIBRARY.RATINGS ("+
	    "RATING_ID INTEGER,"+
	    "RATING VARCHAR(28))";
	DBstatement(sql);
	main.setProgressBarCurrent(task++);
    }
    protected void DBdisconnect() {
        try {
            if (conn != null) {
                DriverManager.getConnection(dblogon+";shutdown=true");
                conn.close();
		conn = null;
	    }           
        }
        catch (SQLException sqlExcept) {

        }
    }    
    protected void DBshutdown() {
        try {
            if (conn != null) {
                DriverManager.getConnection(dbProtocol+";shutdown=true");
                conn.close();
		conn = null;
	    }           
        }
        catch (SQLException sqlExcept) {
        }
    }
}

