/*
 * DB_Interface.java
 *
 * Created on June 27, 2007, 4:39 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elibrary;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import javax.swing.Icon;

/**
 *
 * @author Satalink
 */
public interface Pkg_Interface {
    public boolean License();
    public DB getDB();
    public WebMatrix getWeb();
    public URL getURL(String link);
    public File renameFile(File file);
    public boolean icopyFile(File fromFile, File toFile) throws FileNotFoundException ;
    public boolean ideleteFile(File file);
    public File igetFileFromPath(String Path);
    public int igetRecursiveFileCount(File file);
    public boolean getClipboardAccess();
    public int getSplitPaneDeviderCoord();
    public int getSplitPane2DeviderCoord();
    public Icon getSystemFileIcon(File file);
    public Color[] getColors();
    public Point getWinCoord();
    public Dimension getWinSize();
    public String getDefaultMsgText();
    public float getFileSize(File file);
    public String getUserName();
    public void exitApp();
    public void enableMenu(boolean b);
    public void setMsgArea(String text);
    public void setCursor(String c);
    public URL getFileURL(File file);
    public void setProgressBarMax(int x);
    public void setProgressBarCurrent(int x);
    public void setProgressBarIndeterminate(boolean x);
    public void setSplitPaneDeviderCoord(int width);
    public void setSplitPane2DeviderCoord(int height);
    public void setColors(Color[] colors);      
    public void setWinTitle(String title);
}
