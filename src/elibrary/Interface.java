/*
 * Interface.java
 *
 * Created on June 27, 2007, 4:39 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package elibrary;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import javax.swing.JFileChooser;

/**
 *
 * @author Satalink
 */
public interface Interface {
      public boolean deleteDir(File dir);
      public void exitApp();
      public void getConfigs();
      public Color[] getColors();
      public DB getDB();
      public String getDefaultMsgText();
      public JFileChooser getFileChooser();
      public URL getFileURL(File file);
      public int getSplitPaneDeviderCoord();
      public int getSplitPane2DeviderCoord();
      public URL getURL(String link);
      public String getUserName();
      public Point getWinCoord();
      public Dimension getWinSize();
      public boolean icopyFile (File fromFile, File toFile) throws FileNotFoundException;
      public boolean ideleteFile(File file);
      public File igetFileFromPath(String path);
      public float igetFileSize(File file);
      public int igetRecursiveFileCount(File file);
      public File irenameFile(File file);
      public boolean License();
      public void setColors(Color[] inColors);
      public void setCursor(String c);
      public void setMsgArea(String text);
      public void setProgressBarMax(int x);
      public void setProgressBarCurrent(int x);
      public void setProgressBarIndeterminate(boolean x);
      public void setProgressBarPaintStr(boolean x);
}
